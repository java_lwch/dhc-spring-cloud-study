package com.dhc.student.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_student")
public class Student extends Model<Student> {

    @TableId(value = "classId")
    private Integer classId;

    @TableId(value = "sid")
    private Integer sid;

    @TableField(value = "name")
    private String name;


    @TableField(value = "score")
    private String score;
}
