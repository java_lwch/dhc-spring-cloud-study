package com.dhc.teacher.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.dhc.teacher.dto.TeacherDTO;
import com.dhc.teacher.service.TeacherService;
import com.dhc.teacher.entity.Teacher;
import org.springframework.beans.factory.annotation.Value;
import com.dhc.order.api.IOrderApi;
import com.dhc.order.dto.OrderDTO;

import java.util.List;

@RestController
@Controller
@RefreshScope
@RequestMapping("/teacher")

public class TeacherController {
//    @Value("${server.port}")
//    public Integer port;

    @Autowired
    @Qualifier("teacher1")
    private TeacherService teacherService1;

    @GetMapping()
    public List<Teacher> listTeacher(){
        return teacherService1.listTeacher();
    }

    @GetMapping("/{id}")
    public Teacher getTeacher(@PathVariable("id") Integer id) {
        return teacherService1.getTeacher(id);
    }

    @PostMapping
    public Boolean saveTeacher(@RequestBody TeacherDTO teacherDTO){
        return teacherService1.saveTeacher(teacherDTO);
    }

    @PutMapping
    public Boolean updateTeacher(@RequestBody TeacherDTO teacherDTO){
        return teacherService1.updateTeacher(teacherDTO);
    }

    @DeleteMapping("/{id}")
    public Boolean deleteTeacher(@PathVariable("id")Integer id){
        return teacherService1.deleteTeacher(id);
    }

    @PostMapping("/list")
    public Boolean insertTeacherList(@RequestBody List<TeacherDTO> teacherDTO) {
        return teacherService1.insertTeacherList(teacherDTO);
    }

    @GetMapping("/check/{teacherNo}")
    public Boolean checkTeacherExist(@PathVariable("teacherNo") String teacherNo) {
        return teacherService1.checkTeacherExist(teacherNo);
    }

    @GetMapping("/getOrder/{id}")
    public OrderDTO getOrderInfo(@PathVariable("id") Integer id) {
        return teacherService1.getOrderInfo(id);
    }

    @GetMapping("/getAvgScore/{teacherNo}")
    public int getAvgScore(String teacherNo){
        return teacherService1.getAvgScore(teacherNo);
    }
}
