package com.dhc.scy;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
@EnableFeignClients("com.dhc")
public class ScyApplication {
    public static void main(String[] args) {
        SpringApplication.run(ScyApplication.class, args);
    }
}
