package com.dhc.course.dto;

import lombok.Data;

public class AchievementDTO {

    @Data
    public class CourseDTO {

        private Integer id;
        private Integer NumberOfStudents;
    }
}
