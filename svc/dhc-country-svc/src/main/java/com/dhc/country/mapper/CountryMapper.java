package com.dhc.country.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.country.entity.Country;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CountryMapper extends BaseMapper<Country> {
    /**
     * 查询所有国家的信息
     * @return 所有国家的结果集合
     */
    List<Country> selectCountryList();

    /**
     * 通过国家名字进行模糊查询
     * @param CountryName 国家的名字
     * @return 查询结果的集合
     */
    List<Country> getCountryByName(String CountryName);

    /**
     * 根据国家id进行查询数据
     * @param CountryId 国家Id
     * @return 查询结果的集合
     */
    Country selectCountryById(Integer CountryId);

    /**
     * 根据国家no进行查询数据
     * @param CountryNo 国家No
     * @return 查询结果的集合
     */
    Country selectCountryByNo(Integer CountryNo);
}
