package com.dhc.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.demo.dto.OrderDTO;
import com.dhc.demo.entity.Order;
import com.dhc.demo.mapper.OrderMapper;
import com.dhc.demo.service.OrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author liwencheng
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private ProductServiceImpl productService;

    @Override
    public List<Order> listOrder() {
        return orderMapper.selectOrderList();
    }

    @Override
    public Order getOrder(Integer orderId) {
        return orderMapper.selectById(orderId);
    }

    @Override
    public Boolean saveOrder(OrderDTO orderDTO) {
        Order order = new Order();
        BeanUtils.copyProperties(orderDTO, order);
        return order.insert();
    }

    @Override
    public Boolean updateOrder(OrderDTO orderDTO) {
        Order order = new Order();
        return order.update(new UpdateWrapper<Order>()
                .set("order_no", orderDTO.getOrderNo())
                .set("order_name", orderDTO.getOrderName())
                .eq("id", orderDTO.getId()));
    }

    @Override
    public Boolean deleteOrder(Integer orderId) {
        return super.removeById(orderId);
    }

    @Override
    @Transactional
    public Boolean insertOrderList(List<OrderDTO> orderDTO) {
        saveOrder(orderDTO.get(orderDTO.size() - 1));
        productService.save();
        return true;
        //事务提交或回滚
    }

}
