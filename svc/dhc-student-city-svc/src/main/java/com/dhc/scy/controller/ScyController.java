package com.dhc.scy.controller;

import com.dhc.scy.entity.Student;
import com.dhc.scy.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/scy")
@RefreshScope
public class ScyController {

    @Value("${server.port}")
    private Integer port;

    @Autowired
    StudentService studentService;

    @GetMapping("/{no}")
    String getStudentCity(@PathVariable("no")Integer no){
        String city="大连";

        return port.toString();
    };

    @GetMapping("/getCity")
    Student getStudentCity()
    {
        Integer no=1;
        Student std=studentService.selectStudentCity(no);

        return std;
    }
}
