package com.dhc.country.dto;

import lombok.Data;

@Data
public class CountryDTO {

    private Integer id;
    private String countryNo;
    private String countryName;
}
