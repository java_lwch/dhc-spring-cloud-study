package com.dhc.lzh.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.lzh.entity.City;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface CityMapper extends BaseMapper<CityMapper> {

    City selectCity(Integer no);

}
