package com.dhc.gateway.com.dhc.gateway.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class JWTUtils {
    private  static String TOKEN_KEY;

    public static String getToken(String username){
        JWTCreator.Builder builder = JWT.create();
        builder.withClaim("username",username);
        String token = builder.sign(Algorithm.HMAC256(TOKEN_KEY));

        return token;
    }


    public static DecodedJWT verify(String token){
        return JWT.require(Algorithm.HMAC256(TOKEN_KEY)).build().verify(token);
    }

    @Value("${token.key}")
    private void setTokenKey(String tokenKey){
        JWTUtils.TOKEN_KEY=tokenKey;
    }
}
