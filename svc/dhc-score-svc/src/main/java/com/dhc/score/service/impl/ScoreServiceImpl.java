package com.dhc.score.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.score.dto.ScoreDTO;
import com.dhc.score.entity.Score;
import com.dhc.score.mapper.ScoreMapper;
import com.dhc.score.service.ScoreService;
import com.sun.istack.internal.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author dongjm
 * @version 1.0
 */
@Service
public class ScoreServiceImpl extends ServiceImpl<ScoreMapper, Score> implements ScoreService {

    @Autowired
    private ScoreMapper scoreMapper;

    /**
     * 根据学生sid，课程id 查询成绩
     *
     * @param sid 学生id，cid 课程id
     * @return 分数
     */
    @Override
    public Score selectScoreById(Integer ScoreSid,Integer ScoreCid){
        return scoreMapper.selectScoreById(ScoreSid,ScoreCid);
    }

    /**
     * 根据学生id 查询成绩
     * @param sid 学生id
     * @return 分数的集合
     */
    @Override
    public  List<Score> selectScoreBySid(Integer ScoreSid){
        return scoreMapper.selectScoreBySid(ScoreSid);
    }
}