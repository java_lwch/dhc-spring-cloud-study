package com.dhc.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.course.entity.NumberOfStudentsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface NumberOfStudentsMapper extends BaseMapper<NumberOfStudentsEntity> {

     NumberOfStudentsEntity selectNumberOfStudents(Integer courseId);

}
