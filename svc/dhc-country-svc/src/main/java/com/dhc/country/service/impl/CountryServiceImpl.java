package com.dhc.country.service.impl;

//import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.country.dto.CountryDTO;
import com.dhc.country.entity.Country;
import com.dhc.country.mapper.CountryMapper;
import com.dhc.country.service.CountryService;
//import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class CountryServiceImpl extends ServiceImpl<CountryMapper, Country> implements CountryService {

    @Autowired
    private CountryMapper countryMapper;

    /**
     * 查询所有国家的信息
     * @return 所有国家的结果集合
     */
    @Override
    public List<Country> listCountry() {
        return countryMapper.selectCountryList();
    }

    /**
     * 添加国家的全部信息
     * @param countryDTO 国家的信息
     * @return 是否更新成功
     */
    @Override
    public Boolean saveCountry(CountryDTO countryDTO) {
        Country country = new Country();
        BeanUtils.copyProperties(countryDTO, country);
        return country.insert();
    }

    /**
     * 更新国家的全部信息
     * @param countryDTO 国家的信息
     * @return 是否更新成功
     */
    @Override
    public Boolean updateCountry(CountryDTO countryDTO) {
        Country country = new Country();
        return country.update(new UpdateWrapper<Country>()
                .set("Country_no", countryDTO.getCountryNo())
                .set("Country_name", countryDTO.getCountryName())
                .eq("Country_id", countryDTO.getId()));
    }

    /**
     * 通过国家id进行删除
     * @param CountryId 国家id
     * @return 是否删除成功
     */
    @Override
    public Boolean deleteCountry(Integer CountryId) {

        return super.removeById(CountryId);
    }

    /**
     * 根据国家id进行查询数据
     * @param CountryId 国家Id
     * @return 查询结果的集合
     */
    @Override
    public Country selectCountryById(Integer CountryId){
        return countryMapper.selectCountryById(CountryId);
    }
    /**
     * 通过国家名字进行模糊查询
     * @param CountryName 国家的名字
     * @return 查询结果的集合
     */
    @Override
    public  List<Country> listCountryByName(String CountryName){
        return countryMapper.getCountryByName(CountryName);
    }
}