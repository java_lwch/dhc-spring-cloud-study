package com.dhc.city.api;

import com.dhc.city.dto.CityDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @version 1.0
 */
@FeignClient(name = "dhc-city-svc", path = "/city")
public interface ICityApi {

    /**
     * 查询所有城市的信息
     * @return 所有城市的结果集合
     */
    @GetMapping("/select")
    List<CityDTO> listCity();


    /**
     * 添加城市的全部信息
     * @param cityDTO
     * @return 是否更新成功
     */
    @PostMapping
    Boolean saveCity(@RequestBody CityDTO cityDTO);

    /**
     * 更新城市的全部信息
     * @param cityDTO
     * @return 是否更新成功
     */
    @PutMapping
    Boolean updateCity(@RequestBody CityDTO cityDTO);

    /**
     * 通过城市id进行删除
     * @param CityId 城市id
     * @return 是否删除成功
     */
    @DeleteMapping("/{CityId}")
    Boolean deleteCity(@PathVariable("CityId")Integer CityId);

    /**
     * 根据城市id进行查询数据
     * @param CityId 城市Id
     * @return 查询结果的集合
     */
    @GetMapping("/{CithId}")
    SchoolDTO selectCityById(@PathVariable("CityId")Integer CityId);

    /**
     * 通过城市名字进行模糊查询
     * @param CityName 学校的名字
     * @return 查询结果的集合
     */
    @RequestMapping("/SelectByName/{CityName}")
    List<CityDTO> listCityByName(@PathVariable("CityName")String CityName);
}
