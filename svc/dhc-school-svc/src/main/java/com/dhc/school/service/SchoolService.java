package com.dhc.school.service;

import com.dhc.school.dto.SchoolDTO;
import com.dhc.school.entity.School;

import java.util.List;

public interface SchoolService {

    /**
     * 查询所有学校的信息
     * @return 所有学校的结果集合
     */
    List<School> listSchool();

    /**
     * 添加学校的全部信息
     * @param schoolDTO
     * @return 是否更新成功
     */
    Boolean saveSchool(SchoolDTO schoolDTO);

    /**
     * 更新学校的全部信息
     * @param schoolDTO
     * @return 是否更新成功
     */
    Boolean updateSchool(SchoolDTO schoolDTO);

    /**
     * 通过学校id进行删除
     * @param schoolId 学校id
     * @return 是否删除成功
     */
    Boolean deleteSchool(Integer schoolId);

    /**
     * 根据学校id进行查询数据
     * @param SchoolId 学校Id
     * @return 查询结果的集合
     */
    School selectSchoolById(Integer SchoolId);

    /**
     * 通过学校名字进行模糊查询
     * @param SchoolName 学校的名字
     * @return 查询结果的集合
     */
    List<School> listSchoolByName(String SchoolName);
}
