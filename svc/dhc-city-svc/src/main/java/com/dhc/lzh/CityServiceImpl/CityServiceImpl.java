package com.dhc.lzh.CityServiceImpl;

import com.dhc.lzh.entity.City;
import com.dhc.lzh.mapper.CityMapper;
import com.dhc.lzh.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityMapper cityMapper;

    @Override
    public City selectCity() {
        return selectCity();
    }

    @Override
    public City selectCity(Integer no) {

        return cityMapper.selectCity(no);
    }
}
