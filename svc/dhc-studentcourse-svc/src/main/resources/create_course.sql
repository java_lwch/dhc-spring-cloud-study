CREATE TABLE `course` (
  `course_id` int(4) NOT NULL,
  `course_name` char(20) NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci