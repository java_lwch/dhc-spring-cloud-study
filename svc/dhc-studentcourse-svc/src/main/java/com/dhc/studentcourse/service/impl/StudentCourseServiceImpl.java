package com.dhc.studentcourse.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.course.api.ICourseApi;
import com.dhc.course.dto.CourseDTO;
import com.dhc.student.api.IStudentApi;
import com.dhc.student.dto.StudentDTO;
import com.dhc.studentcourse.entity.CourseEntity;
import com.dhc.studentcourse.entity.StudentEntity;
import com.dhc.studentcourse.mapper.StudentCourseMapper;
import com.dhc.studentcourse.entity.StudentCourseEntity;
import com.dhc.studentcourse.service.StudentCourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentCourseServiceImpl extends ServiceImpl<StudentCourseMapper, StudentCourseEntity> implements StudentCourseService {

    @Autowired
    private StudentCourseMapper studentCourseMapper;

    @Autowired
    private IStudentApi studentApi;

    @Autowired
    private ICourseApi courseApi;

    @Override
    public List<StudentCourseEntity> listStudentCourse() {
        return studentCourseMapper.selectStudentCourseList();
    }

    @Override
    public StudentCourseEntity getStudentCourse(Integer studentId) {
        return studentCourseMapper.selectById(studentId);
    }

    @Override
    public List<StudentCourseEntity> getStudentCourseList(Integer studentId) {
        return studentCourseMapper.selectStudentCourseList1(studentId);
    }

    @Override
    public Boolean insertStudentCourse(StudentCourseEntity studentCourseEntity) {
        StudentCourseEntity studentCourse = new StudentCourseEntity();
        BeanUtils.copyProperties(studentCourseEntity, studentCourse);
        return studentCourse.insert();
    }

    @Override
    public Boolean updateStudent(Integer studentId , Integer courseId , String courseName) {
        //学生是否存在
//        StudentEntity studentEntity = new StudentEntity().selectById(studentId);
        // 调用学生api，check学生是否存在
        StudentDTO studentDTO = studentApi.getUser(studentId);
//        if(studentEntity == null){
        if(studentDTO == null){
            System.out.println("学生不存在！");
            return false;
        }else {
            //课程存在check
//            CourseEntity courseEntity = new CourseEntity().selectById(courseId);
            //调用课程api，check课程是否存在
            CourseDTO courseDto = courseApi.getCourse(courseId);
//            if (courseEntity == null) {
            if (courseDto == null) {
                System.out.println("课程不存在！");
                return false;
            }else {
                try {
                    return studentCourseMapper.updateStudent(studentId, courseId, courseName);
                } catch (Exception e) {
                    System.out.println("更新失败！！！");
                    return false;
                }
            }
        }
    }

    /**
     * 根据学生id，课程id 查询学生信息，课程信息
     * check后，登录
     * @param studentId 学生id ，courseId 课程id
     * @return true/false
     */
    @Override
    public Boolean insertStudent(Integer studentId , Integer courseId) {
        //学生课程信息是否存在
        StudentCourseEntity studentCourseEntity = studentCourseMapper.selectStudentCourse(studentId,courseId);
        if(studentCourseEntity != null){
            System.out.println("学生课程信息已存在！登录失败");
            return false;
        }else{
            //登录的学生是否存在
            StudentEntity studentEntity = new StudentEntity().selectById(studentId);
            if(studentEntity == null){
                System.out.println("学生不存在！请重新登陆");
                return false;
            }else{
                //登录的课程是否存在
                CourseEntity courseEntity = new CourseEntity().selectById(courseId);
                if (courseEntity == null){
                    System.out.println("课程不存在！请重新登陆");
                    return false;
                }else{
                    //存在的场合，学生课程表登录
                    try{
                        return studentCourseMapper.insertStudent(studentId,courseId,studentEntity.getStudentName(),studentEntity.getStudentSex(),studentEntity.getStudentClass(),courseEntity.getCourseName());
                    }catch(Exception e){
                        System.out.println("登录失败！！！");
                        return false;
                    }
                }
            }
        }
    }


    @Override
    public Boolean updateStudentCourse(StudentCourseEntity studentCourseEntity) {
        StudentCourseEntity course = new StudentCourseEntity();
        return course.update(new UpdateWrapper<StudentCourseEntity>()
                .set("course_name", studentCourseEntity.getCourseName())
                .eq("student_id", studentCourseEntity.getStudentId())
                .eq("course_id", studentCourseEntity.getCourseId()));
    }

    /**
     * 根据学生id，删除学生课程信息
     *
     * @param studentId 学生id
     * @return ture/false
     */
    @Override
    public Boolean deleteStudentCourse(Integer studentId) {
        return super.removeById(studentId);
    }

}
