package com.dhc.banji.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.banji.dto.ClassesDto;
import com.dhc.banji.entity.ClassesEntity;
import com.dhc.banji.mapper.ClassesMapper;
import com.dhc.banji.service.ClassesService;
import com.dhc.school.api.ISchoolApi;
import com.dhc.school.dto.SchoolDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClassesServiceImpl extends ServiceImpl<ClassesMapper, ClassesEntity>implements ClassesService {
    @Autowired
    private ClassesMapper classesMapper;

    @Autowired
    private ISchoolApi iSchoolApi;

    @Override
    public List<ClassesEntity> listClasses() {
        return classesMapper.selectClassesList();
    }

    @Override
    public List<ClassesEntity> getClasses(Integer classesId) {
        return classesMapper.selectById(classesId);
    }

    @Override
    public Boolean deleteClasses(Integer classesId) {
        return super.removeById(classesId);
    }

    @Override
    public Boolean updateClasses(ClassesEntity classesEntity) {
        ClassesEntity classesEntity1 = new ClassesEntity();
        return classesEntity1.update(new UpdateWrapper<ClassesEntity>()
                .set("classes_name", classesEntity.getClassesName())
                .set("school_id", classesEntity.getSchoolId())
                .eq("classes_id", classesEntity.getClassesId()));
    }

    @Override
    public Boolean saveClasses(ClassesEntity classesEntity) {
        ClassesEntity classesEntity1 = new ClassesEntity();
        BeanUtils.copyProperties(classesEntity, classesEntity1);
        return classesEntity1.insert();
    }

    @Override
//    @Transactional
    public Boolean insertClassesList(List<ClassesEntity> classesEntity) {
        ClassesEntity classesEntity1 = new ClassesEntity();
        for (int i = 0; i < classesEntity.size(); i++) {
            BeanUtils.copyProperties(classesEntity.get(i), classesEntity1);
            SchoolDTO schoolDTO=iSchoolApi.selectSchoolById(classesEntity1.getSchoolId());
            if(schoolDTO==null){
                System.out.println("School is not exist!!");
            }else {
                classesEntity1.insert();
            }
        }
        return true;
    }

    @Override
    public Integer getMaxClassesOfSchool(Integer classesId) {
        return classesMapper.getMaxClassesOfSchool();
    }

    @Override
    public Double selectAVGScoreByTeacher(int teacher_id){
        return classesMapper.selectAVGScoreByTeacher(teacher_id);
    }

    @Override
    public String getStudentNameLower60(int class_id){
        return classesMapper.getStudentNameLower60(class_id);
    }
}
