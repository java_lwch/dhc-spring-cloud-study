package com.dhc.banji.service;

import com.dhc.banji.dto.ClassesDto;
import com.dhc.banji.entity.ClassesEntity;

import java.util.List;

public interface ClassesService {
    List<ClassesEntity> listClasses() ;

//    ClassesEntity getClasses(Integer classesId);

      List<ClassesEntity> getClasses(Integer classesId);

      Boolean saveClasses(ClassesEntity classesEntity);

      Boolean updateClasses(ClassesEntity classesEntity);

      Boolean deleteClasses(Integer classesId);

      Boolean insertClassesList(List<ClassesEntity> classesEntity);

      Integer getMaxClassesOfSchool(Integer classesId);

      Double selectAVGScoreByTeacher(int teacher_id);

      String getStudentNameLower60(int class_id);
}
