package com.dhc.user.api;

import com.dhc.user.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author liwencheng
 */
@FeignClient(value = "dhc-user-svc")
public interface IUserApi {

    @GetMapping("/{id}")
    UserDTO getUser(Integer id);
}
