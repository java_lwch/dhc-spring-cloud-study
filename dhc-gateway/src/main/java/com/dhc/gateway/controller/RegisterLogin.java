package com.dhc.gateway.controller;

import com.dhc.gateway.com.dhc.gateway.utils.JWTUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.WebSession;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
public class RegisterLogin {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @PostMapping("/user/register")
    public Map<String,Object> register(@RequestParam(value="username") String username, @RequestParam(value="password") String password){
        Map<String,Object> map = new HashMap<>();
        if("".equals(username) || "".equals(password)){
            map.put("state",false);
            map.put("msg","用户名，密码不能为空");
        }else {
//            stringRedisTemplate.opsForValue().set("USERNAME", username);
//            stringRedisTemplate.opsForValue().set("PASSWORD", password);
            stringRedisTemplate.opsForValue().set(username,password);
            map.put("state",true);
            map.put("msg","注册成功，请登录:http://localhost:9999/user/login");
        }

        return map;
    }

    @PostMapping("/user/login")
    public Map<String,Object> login(@RequestParam(value="username") String username, @RequestParam(value="password") String password) {
        Map<String, Object> map = new HashMap<>();
//        if (stringRedisTemplate.opsForValue().get("USERNAME").equals(username) &&
//                stringRedisTemplate.opsForValue().get("PASSWORD").equals(password)) {
        if(stringRedisTemplate.opsForValue().get(username)==null){
            map.put("state",false);
            map.put("msg","该用户未注册，请注册：http://localhost:9999/user/register");
            return map;
        }
        if(stringRedisTemplate.opsForValue().get(username).equals(password)){
            //login success 生成JWT token,生成完返回给客户端
            String token;
            token = JWTUtils.getToken(username);
            map.put("state", true);
            map.put("msg", "登录成功");
            map.put("token", token);
        } else {
            map.put("state", false);
            map.put("msg", "用户名，密码不对");
        }

        return map;
    }
}
