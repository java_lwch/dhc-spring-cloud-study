package com.dhc.role.service;

import com.dhc.role.entity.Role;
import com.dhc.role.dto.RoleDto;

public interface RoleService {

    /**
     * 添加学校的全部信息
     * @param roleDto
     * @return 是否更新成功
     */
    Boolean saveRole(RoleDto roleDto);

    /**
     * 更新学校的全部信息
     * @param roleDto
     * @return 是否更新成功
     */
    Boolean updateRole(RoleDto roleDto);

    /**
     * 通过学校id进行删除
     * @param roleId 学校id
     * @return 是否删除成功
     */
    Boolean deleteRole(Integer roleId);

    /**
     * 根据学校id进行查询数据
     * @param RoleId 学校Id
     * @return 查询结果的集合
     */
    Role selectRoleById(Integer RoleId);
}
