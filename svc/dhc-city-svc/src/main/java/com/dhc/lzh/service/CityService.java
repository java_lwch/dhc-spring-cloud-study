package com.dhc.lzh.service;

import com.dhc.lzh.entity.City;

import java.util.List;

public interface CityService {

    //查询城市
    City selectCity(Integer no);
}
