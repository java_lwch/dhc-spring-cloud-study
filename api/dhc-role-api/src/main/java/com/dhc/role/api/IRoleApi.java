package com.dhc.role.api;

import com.dhc.role.dto.RoleDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "dhc-role-svc", path = "/role")
public interface IRoleApi {

    /**
     * 添加学校的全部信息
     * @param roleDto
     * @return 是否更新成功
     */
    @PostMapping
    Boolean saveRole(@RequestBody RoleDto roleDto);

    /**
     * 更新学校的全部信息
     * @param roleDto
     * @return 是否更新成功
     */
    @PutMapping
    Boolean updateRole(@RequestBody RoleDto roleDto);

    /**
     * 通过学校id进行删除
     * @param RoleId 学校id
     * @return 是否删除成功
     */
    @DeleteMapping("/{RoleId}")
    Boolean deleteRole(@PathVariable("RoleId")Integer RoleId);

    /**
     * 根据学校id进行查询数据
     * @param RoleId 学校Id
     * @return 查询结果的集合
     */
    @GetMapping("/{RoleId}")
    RoleDto selectRoleById(@PathVariable("RoleId")Integer RoleId);
}
