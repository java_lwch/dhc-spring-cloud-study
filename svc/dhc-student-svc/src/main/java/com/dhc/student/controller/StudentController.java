package com.dhc.student.controller;

import com.dhc.student.dto.StudentAvgScore;
import com.dhc.student.dto.StudentDTO;
import com.dhc.student.entity.Student;
import com.dhc.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.util.List;

@RestController
@RequestMapping("/Student")

public class StudentController {
    @Autowired
    private StudentService studentService;
//    调用成绩接口
//    @Autowired
//    private IChengjiApi iChengjiApi;

    @GetMapping()
    public List<Student> listStudent(){
        return studentService.listStudent();
    }

    @GetMapping("/{sId}")
    public Student getOrder(@PathVariable("sId")Integer sId){
        return studentService.getStudent(sId);
    }

    @PostMapping
    public Boolean saveOrder(@RequestBody StudentDTO studentDTO){
        return studentService.saveStudent(studentDTO);
    }

    @PutMapping
    public Boolean updateOrder(@RequestBody StudentDTO studentDTO){
        return studentService.updateStudent(studentDTO);
    }

    @DeleteMapping("/{sId}")
    public Boolean deleteOrder(@PathVariable("sId")Integer sId){
        return studentService.deleteStudent(sId);
    }

    @PostMapping("/list")
    public Boolean insertOrderList(@RequestBody List<StudentDTO> studentDTO){
        return studentService.insertStudentList(studentDTO);
    }

    @PostMapping(("/SlelectByName/{studentName}"))
    public StudentAvgScore selectByName(@PathVariable("studentName") String name){
        // 平均分数
        String avgScore = null;
        //当传入参数name = "张"时取得所有名字带张的学生信息
        List<Student> list = studentService.selectByName(name);
        if (list != null && list.size() != 0)
        {
            //名字中带“张”字的学生的总分
            float count = 0;
            for (Student s: list) {
                //假设成绩接口中提供方法getChengjiCount()通过学生id获得该学生总分
                //调用成绩接口取得每个学生的总成绩
                // count += iChengjiApi.getChengjiCount(s.getSid());
            }
            //查询名字中带“张”字的学生的总分平均成绩
            DecimalFormat dF = new DecimalFormat("0.000");
            avgScore = dF.format((float)count/list.size());

        }
        StudentAvgScore studentAvgScore = new StudentAvgScore();
        studentAvgScore.setScore(avgScore);

        return studentAvgScore;
    }


}
