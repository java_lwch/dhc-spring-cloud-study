package com.dhc.dorm.api;

import com.dhc.dorm.dto.DormDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/10 23:54
 */
@FeignClient(name = "dhc-dorm-svc", path = "/dorm")
public interface IDormApi {
    /**
     * 通过id 查询宿舍
     * @param dorm_id
     * @return 宿舍信息
     */
    @GetMapping("/{dorm_id}")
    DormDTO getDorm(@PathVariable("dorm_id") Integer dorm_id);
}
