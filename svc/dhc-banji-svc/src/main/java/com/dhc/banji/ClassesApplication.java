package com.dhc.banji;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringCloudApplication
@EnableFeignClients("com.dhc")
public class ClassesApplication {
    public static void main(String[] args) {
        SpringApplication.run(ClassesApplication.class,args);
    }
}
