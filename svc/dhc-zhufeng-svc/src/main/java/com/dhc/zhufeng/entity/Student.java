package com.dhc.zhufeng.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
@TableName("student")
public class Student extends Model<Student> {


    @TableField("no")
    private Integer no;

    @TableField("name")
    private String name;

    @TableField("math")
    private Integer math;

    @TableField("chinese")
    private Integer chinese;

    @TableField("english")
    private Integer english;

    @TableField("sum")
    private Integer sum;

}
