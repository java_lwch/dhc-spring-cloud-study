package com.dhc.scy.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("student")
public class Student extends Model<Student> {

    //学号
    @TableField("no")
    private Integer no;

    //姓名
    @TableField("name")
    private String name;

    //学号
    @TableField("age")
    private Integer age;

    //年龄
    @TableField("city")
    private String city;
}
