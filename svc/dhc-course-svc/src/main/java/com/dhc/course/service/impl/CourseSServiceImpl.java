package com.dhc.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.course.entity.CourseEntity;
import com.dhc.course.entity.NumberOfStudentsEntity;
import com.dhc.course.mapper.CourseMapper;
import com.dhc.course.mapper.NumberOfStudentsMapper;
import com.dhc.course.service.CourseSService;
import com.dhc.course.service.CourseService;
import com.dhc.school.api.ISchoolApi;
import com.dhc.school.dto.SchoolDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CourseSServiceImpl extends ServiceImpl<CourseMapper, CourseEntity>implements CourseSService {
    @Autowired
    private CourseMapper courseMapper;

    private ISchoolApi iSchoolApi;

    @Override
    public List<CourseEntity> listCourse() {
        return courseMapper.selectCourseList();
    }

    @Override
    public List<CourseEntity> getCourse(Integer courseId) {
        return courseMapper.selectById(courseId);
    }

    @Override
    public Boolean deleteCourse(Integer CourseId) {
        return super.removeById(CourseId);
    }

    @Override
    public Boolean updateCourse(CourseEntity courseEntity) {
        CourseEntity courseEntity1 = new CourseEntity();
        return courseEntity1.update(new UpdateWrapper<CourseEntity>()
                .set("course_name", courseEntity.getCourseName())
                .set("school_id", courseEntity.getSchoolId())
                .eq("course_id", courseEntity.getCourseId()));
    }

    @Override
    public Boolean saveCourse(CourseEntity courseEntity) {
        CourseEntity courseEntity1 = new CourseEntity();
        BeanUtils.copyProperties(courseEntity, courseEntity1);
        return courseEntity1.insert();
    }
    @Override
//    @Transactional
    public Boolean insertCourseList(List<CourseEntity> courseEntity) {
        CourseEntity courseEntity1 = new CourseEntity();
        for(int i =0;i < courseEntity.size();i++){
            BeanUtils.copyProperties(courseEntity.get(i), courseEntity1);
            SchoolDTO schoolDTO=iSchoolApi.selectSchoolById(courseEntity1.getSchoolId());
            if(schoolDTO==null){
                System.out.println("School is not exist!");
            }else {
                courseEntity1.insert();
            }
        }
        return true;
    }

}


