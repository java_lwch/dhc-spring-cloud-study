package com.dhc.product.controller;

import com.dhc.product.dto.ProductDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liwencheng
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Value("${server.port}")
    public Integer port;

    /**
     * 根据id 查询商品
     *
     * @param id 商品id
     * @return 商品信息
     */
    @GetMapping("/{id}")
    public ProductDTO getProduct(@PathVariable("id") Integer id) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(1);
        productDTO.setProductNo(String.valueOf(port));
        productDTO.setProductName(String.valueOf(port));
        return productDTO;
    }
}
