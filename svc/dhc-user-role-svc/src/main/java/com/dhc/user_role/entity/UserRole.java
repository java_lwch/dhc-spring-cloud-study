package com.dhc.user_role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author sunzhs
 */
@EqualsAndHashCode(callSuper = true)
@TableName("t_user_role")
@Data
public class UserRole extends Model<UserRole> {

    //用户id
    @TableField("user_id")
    private Integer userId;
    //角色id
    @TableField("role_id")
    private Integer roleId;
    //更新时间
    @TableField("update_time")
    private Date updateTime;
    //添加时间
    @TableField("add_time")
    private Date addTime;

}
