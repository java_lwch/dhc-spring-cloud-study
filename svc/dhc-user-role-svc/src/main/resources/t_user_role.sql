CREATE TABLE `t_user_role`  (
                                         `user_id` int(11) NOT NULL,
                                         `role_id` int(11) NOT NULL,
                                         `update_time` datetime(0) NOT NULL,
                                         `add_time` datetime(0) NOT NULL,
                                         PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;