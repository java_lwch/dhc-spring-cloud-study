package com.dhc.zhufeng.api;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "dhc-zhufeng-svc")
public interface IStudentApi {

    @GetMapping("/{zf}")
    String getNum();
}
