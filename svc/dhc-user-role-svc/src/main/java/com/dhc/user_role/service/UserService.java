package com.dhc.user_role.service;

import com.dhc.user_role.entity.User;
import com.dhc.user_role.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author sunzhs
 */

public interface UserService {
    /**
     * 根据指定的用户id
     * @param id 用户id
     * @return 指定用户id的用户信息
     */
    User getUserById(Integer id);
}
