package com.dhc.demo.controller;

import com.alibaba.fastjson.JSON;
import com.dhc.demo.dto.OrderDTO;
import com.dhc.demo.service.HelloService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author liwencheng
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class HelloControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    @Qualifier("hello1")
    private HelloService hello1Service;

    @Before
    public void init() throws IOException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void destroy() {

    }

    @Test
    public void testGetOrder() throws Exception {
        when(hello1Service.test()).thenReturn("AAA");
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setOrderNo("orderDTO");
        mockMvc.perform(get(
                "/hello/test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON.toJSONString(orderDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("AAA")));
    }
}
