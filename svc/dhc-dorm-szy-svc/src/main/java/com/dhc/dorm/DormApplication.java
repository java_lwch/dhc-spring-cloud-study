package com.dhc.dorm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/10 23:27
 */
@SpringCloudApplication
@EnableFeignClients("com.dhc")
@MapperScan("com.dhc.dorm.mapper")
public class DormApplication {
    public static void main(String[] args) {
        SpringApplication.run(DormApplication.class, args);
    }
}
