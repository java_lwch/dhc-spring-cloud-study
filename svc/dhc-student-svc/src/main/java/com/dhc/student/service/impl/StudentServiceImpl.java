package com.dhc.student.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.student.dto.StudentDTO;
import com.dhc.student.entity.Student;
import com.dhc.student.mapper.StudentMapper;
import com.dhc.student.service.StudentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {
    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private StudentServiceImpl studentService;

    @Override
    public List<Student> listStudent() {
        return studentMapper.selectStudentList();
    }

    @Override
    public Student getStudent(Integer sId) {
        return studentMapper.selectById(sId);
    }

    @Override
    public Boolean saveStudent(StudentDTO studentDTO) {
        Student student = new Student();
        BeanUtils.copyProperties(studentDTO, student);
        return student.insert();
    }

    @Override
    public Boolean updateStudent(StudentDTO studentDTO) {
        Student student = new Student();
        return student.update(new UpdateWrapper<Student>()
                .set("sid", studentDTO.getSid())
                .set("name", studentDTO.getName())
                .eq("id", studentDTO.getClassId()));
    }

    @Override
    public Boolean deleteStudent(Integer sId) {
        return super.removeById(sId);
    }

    @Override
    @Transactional
    public Boolean insertStudentList(List<StudentDTO> studentDTO) {
        saveStudent(studentDTO.get(studentDTO.size() - 1));
        return true;
    }

    @Override
    public List<Student> selectByName(String name) {
        List<Student> list = studentMapper.selectStudentByNameList("张");

        return list;
    }
}
