package com.dhc.course.controller;

import com.dhc.course.dto.CourseDTO;
import com.dhc.course.entity.NumberOfStudentsEntity;
import com.dhc.course.entity.CourseEntity;
import com.dhc.course.service.CourseSService;
import com.dhc.course.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {

    @Autowired
    private CourseSService courseSService;
    private CourseService  courseService;

    @Value("${server.port}")
    public Integer port;

    /**
     * 根据id 查询课程
     *
     * @param id 课程id
     * @return 课程信息
     */
//    @GetMapping("/{id}")

//    public CourseDTO getCourse(@PathVariable("id") Integer id) {
//        CourseDTO courseDTO = new CourseDTO();
//    //    courseDTO.setCourseId(port);
//    //    courseDTO.setCourseName(String.valueOf(port));
//        courseDTO.(port);
//        return courseDTO;
//    }

    /**
     * 根据课程id 查询指定课程在60分以上的人数
     * @param id 课程id
     * @return 人数信息
     */
    @GetMapping("/{id}")
    public NumberOfStudentsEntity getStudentCourseList(@PathVariable("id") Integer id){
        return courseService.getNumberOfStudents(id);
    }

    @GetMapping()
    public List<CourseEntity> listCourse(){
        return courseSService.listCourse();
    }

    @GetMapping("/{courseId}")
    public List<CourseEntity> getCourse(@PathVariable("courseId")Integer courseId){
        return courseSService.getCourse(courseId);
    }

    @DeleteMapping("/{courseId}")
    public Boolean deleteCourse(@PathVariable("courseId")Integer courseId){
        return courseSService.deleteCourse(courseId);
    }

    @PutMapping
    public Boolean updateCourse(@RequestBody CourseEntity courseEntity){
        return courseSService.updateCourse(courseEntity);
    }

    @PostMapping
    public Boolean saveCourse(@RequestBody CourseEntity courseEntity){
        return courseSService.saveCourse(courseEntity);
    }

    @PostMapping("/list")
    public Boolean insertCourseList(@RequestBody List<CourseEntity> courseEntity){
        return courseSService.insertCourseList(courseEntity);
    }
}
