package com.dhc.score.service;

import com.dhc.score.dto.ScoreDTO;
import com.dhc.score.entity.Score;

import java.util.List;

public interface ScoreService {

    /**
     * 根据学生sid，课程id 查询成绩
     *
     * @param sid 学生id，cid 课程id
     * @return 分数
     */
    Score selectScoreById(Integer ScoreSid,Integer ScoreCid);

    /**
     * 根据学生id 查询成绩
     * @param sid 学生id
     * @return 分数的集合
     */
    List<Score>  selectScoreBySid(Integer ScoreSid);
}
