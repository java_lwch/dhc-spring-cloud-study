package com.dhc.scy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.scy.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface StudentMapper extends BaseMapper<StudentMapper> {

    Student selectStudentCity(Integer no);

}
