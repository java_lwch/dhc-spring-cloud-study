package com.dhc.student.dto;

import lombok.Data;

@Data
public class StudentDTO {

    private int studentId;
    private String studentName;
    private String studentGender;
    private int studentClass;
    private int studentDorm;
    private String cityName;
    private double studentPoint;
}
