package com.dhc.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/13 0:47
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_teacher")
public class Teacher extends Model<Teacher> {
    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    @TableField(value = "teacher_no")
    private String teacherno;

    @TableField(value = "teacher_name")
    private String teachername;

    @TableField(value = "teacher_sex")
    private String teachersex;

    @TableField(value = "teacher_age")
    private String teacherage;

    @TableField(value = "teacher_salary")
    private String teachersalary;

    @TableField(value = "school_no")
    private String schoolno;
}
