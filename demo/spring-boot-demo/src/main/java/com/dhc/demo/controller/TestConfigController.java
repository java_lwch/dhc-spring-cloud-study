package com.dhc.demo.controller;

import com.dhc.demo.components.MyConfiguration;
import com.dhc.demo.dto.OrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * @author liwencheng
 */
@RestController
@RequestMapping("/testConfig")
public class TestConfigController {

    @Autowired
    private OrderDTO temp;

    @Autowired
    private MyConfiguration myConfiguration;

    @Value("${dhc.test.name}")
    private String myName;

    @Value("${dhc.test.value}")
    private String myValue;

    @GetMapping("/testAutoConfig")
    public String testAutoConfig(){
        return "no: " + temp.getOrderNo() + " name: " + temp.getOrderName();
    }

    @GetMapping("/testBeanConfig")
    public String testBeanConfig(){
        return "name: " + myConfiguration.getName() + " name: " + myConfiguration.getValue();
    }

    @GetMapping("/testValueConfig")
    public String testValueConfig(){
        return "name: " + myName + " name: " + myValue;
    }
}
