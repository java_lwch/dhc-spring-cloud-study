package com.dhc.zhufeng.ServiceImpl;


import com.dhc.zhufeng.mapper.StudentMapper;
import com.dhc.zhufeng.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentMapper studentMapper;

    @Override
    public Integer selectCha() {

        return studentMapper.selectCha();
    }
}
