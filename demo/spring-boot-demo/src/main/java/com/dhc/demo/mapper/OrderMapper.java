package com.dhc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.demo.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liwencheng
 */
@Mapper
@Repository
public interface OrderMapper extends BaseMapper<Order> {

    List<Order> selectOrderList();
}
