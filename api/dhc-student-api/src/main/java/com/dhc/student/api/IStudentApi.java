package com.dhc.student.api;

import com.dhc.student.dto.StudentAvgScore;
import com.dhc.student.dto.StudentDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(value = "dhc-student-svc", path = "/student")
public interface IStudentApi {
    @GetMapping("/{id}")
    StudentDTO getUser(@PathVariable("id")Integer Id);


    /**
     * 通过学生名字进行模糊查询学生并算出学生总成绩的平均成绩
     * @param name 学生的名字
     * @return 符合条件的学生总分平均成绩
     */

    @GetMapping("/SlelectByName/{studentName}")
    StudentAvgScore getAvgScoreByName(@PathVariable("name")String name);
}
