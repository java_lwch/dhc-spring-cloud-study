package com.dhc.demo.dto;

import lombok.Data;

/**
 * @author liwencheng
 */
@Data
public class OrderDTO {

    private Integer id;

    private String orderNo;

    private String orderName;


}
