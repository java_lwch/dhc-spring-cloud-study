package com.dhc.manager.service;

import com.dhc.manager.dto.ManagerDTO;
import com.dhc.manager.entity.Manager;

import java.util.List;

public interface ManagerService {

     List<Manager> selectAll();

     Boolean deleteManager(Integer managerId);

     Boolean updateManager(ManagerDTO managerDTO);

     Boolean saveManager(ManagerDTO managerDTO);


}
