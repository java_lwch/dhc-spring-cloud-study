package com.dhc.zhufeng.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.zhufeng.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface StudentMapper {

    Integer selectCha();
}
