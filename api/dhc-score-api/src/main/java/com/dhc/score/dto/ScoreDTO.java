package com.dhc.score.dto;

import lombok.Data;

/**
 * @author dongjianmin
 */
@Data
public class ScoreDTO {

    private Integer ScoreSid;    //学生sid
    private Integer ScoreCid;    //课程cid
    private Integer ScoreScores; //考试分数
}
