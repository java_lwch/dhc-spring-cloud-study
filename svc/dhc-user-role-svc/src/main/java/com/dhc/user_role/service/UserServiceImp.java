package com.dhc.user_role.service;

import com.dhc.user_role.entity.User;
import com.dhc.user_role.mapper.UserMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Data
public class UserServiceImp implements UserService {

    @Autowired
    public UserMapper userMapper;

    /**
     * 根据用户id查询用户信息
     * @param id 用户id
     * @return 用户信息
     */
    @Transactional
    @Override
    public User getUserById(Integer id) {
        return userMapper.getUserById(id);
    }
}
