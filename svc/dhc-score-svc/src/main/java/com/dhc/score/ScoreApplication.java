package com.dhc.score;


import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author dongjm
 */
@SpringCloudApplication
public class ScoreApplication {
    public static void main(String[] args) {

        SpringApplication.run(ScoreApplication.class, args);
    }
}
