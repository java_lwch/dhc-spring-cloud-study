package com.dhc.product.fallback;

import com.dhc.product.api.IProductApi;
import com.dhc.product.dto.ProductDTO;
import feign.hystrix.FallbackFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * @author liwencheng
 */
@Component
public class HystrixFallbackFactory implements FallbackFactory<IProductApi> {

    @Override
    public IProductApi create(Throwable throwable) {
        throwable.printStackTrace();
        return new IProductApi(){
            /**
             * 根据id 查询商品
             *
             * @param id 商品id
             * @return 商品信息
             */
            @Override
            public ProductDTO getProduct(Integer id) {
                ProductDTO productDTO = new ProductDTO();
                productDTO.setProductName("HystrixFallbackFactory");
                productDTO.setProductNo("HystrixFallbackFactory");
                return productDTO;
            }
        };
    }
}
