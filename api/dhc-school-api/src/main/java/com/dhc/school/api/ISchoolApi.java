package com.dhc.school.api;

import com.dhc.school.dto.SchoolDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author HanwenZhang
 * @version 1.0
 */
@FeignClient(name = "dhc-school-svc", path = "/school")
public interface ISchoolApi {

    /**
     * 查询所有学校的信息
     * @return 所有学校的结果集合
     */
    @GetMapping("/select")
    List<SchoolDTO> listSchool();


    /**
     * 添加学校的全部信息
     * @param schoolDTO
     * @return 是否更新成功
     */
    @PostMapping
    Boolean saveSchool(@RequestBody SchoolDTO schoolDTO);

    /**
     * 更新学校的全部信息
     * @param schoolDTO
     * @return 是否更新成功
     */
    @PutMapping
    Boolean updateSchool(@RequestBody SchoolDTO schoolDTO);

    /**
     * 通过学校id进行删除
     * @param SchoolId 学校id
     * @return 是否删除成功
     */
    @DeleteMapping("/{SchoolId}")
    Boolean deleteSchool(@PathVariable("SchoolId")Integer SchoolId);

    /**
     * 根据学校id进行查询数据
     * @param SchoolId 学校Id
     * @return 查询结果的集合
     */
    @GetMapping("/{SchoolId}")
    SchoolDTO selectSchoolById(@PathVariable("SchoolId")Integer SchoolId);

    /**
     * 通过学校名字进行模糊查询
     * @param SchoolName 学校的名字
     * @return 查询结果的集合
     */
    @RequestMapping("/SelectByName/{SchoolName}")
    List<SchoolDTO> listSchoolByName(@PathVariable("SchoolName")String SchoolName);
}
