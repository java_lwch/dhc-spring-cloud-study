package com.dhc.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.role.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface RoleMapper extends BaseMapper<Role> {
    Role selectRoleById(Integer RoleId);
}
