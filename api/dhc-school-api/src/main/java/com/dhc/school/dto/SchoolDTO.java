package com.dhc.school.dto;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.print.attribute.standard.Chromaticity;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Data
public class SchoolDTO {

    private Integer SchoolId;
    private String SchoolName;
    private String SchoolType;
    private String SchoolAddress;
    private String SchoolEmail;
    private Integer cityId;
}
