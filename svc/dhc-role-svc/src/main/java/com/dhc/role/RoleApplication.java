package com.dhc.role;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringCloudApplication
@EnableFeignClients("com.dhc")
@MapperScan("com.dhc.role.mapper")
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class RoleApplication {
    public static void main(String[] args) {
        SpringApplication.run(RoleApplication.class,args);
    }
}
