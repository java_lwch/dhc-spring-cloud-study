package com.dhc.role.controller;

import com.dhc.role.dto.RoleDto;
import com.dhc.role.entity.Role;
import com.dhc.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 查询角色
     * @param RoleId 角色Id
     * @return 查询结果的集合
     */
    @GetMapping("/{RoleId}")
    public Role selectRoleById(@PathVariable("RoleId")Integer RoleId){
        return roleService.selectRoleById(RoleId);
    }

    /**
     * 添加角色
     * @param roleDto
     * @return 是否添加成功
     */
    @PostMapping
    public Boolean saveRole(@RequestBody RoleDto roleDto){
        return roleService.saveRole(roleDto);
    }

    /**
     * 更新角色
     * @param roleDto
     * @return 是否更新成功
     */
    @PutMapping
    public Boolean updateRole(@RequestBody RoleDto roleDto){
        return roleService.updateRole(roleDto);
    }

    /**
     * 删除角色
     * @param RoleId 角色id
     * @return 是否删除成功
     */
    @DeleteMapping("/{RoleId}")
    public Boolean deleteRole(@PathVariable("RoleId")Integer RoleId){
        return roleService.deleteRole(RoleId);
    }




}
