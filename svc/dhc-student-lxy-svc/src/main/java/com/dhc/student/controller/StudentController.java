package com.dhc.student.controller;

import com.dhc.student.entity.Student;
import com.dhc.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/student")
@RefreshScope
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/selectStudentName/{studentName}")
    public Double selectAvgPoint(@PathVariable("studentName")String studentName){
        return studentService.selectAvgPoint(studentName);
    }
}