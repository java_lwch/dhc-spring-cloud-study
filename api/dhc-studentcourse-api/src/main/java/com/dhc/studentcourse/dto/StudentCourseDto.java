package com.dhc.studentcourse.dto;


import lombok.Data;

@Data
public class StudentCourseDto {
    private Integer studentId;
    private String studentName;
    private String studentSex;
    private String studentClass;
    private String courseId;
    private String courseName;
}
