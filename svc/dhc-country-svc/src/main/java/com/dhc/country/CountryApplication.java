package com.dhc.country;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.swing.*;

@SpringCloudApplication  //包含@SpringBootApplication注解，注册服务中心注解 断路器注解
@EnableFeignClients("com.dhc.country")  //扫描和注册feign客户端bean定义
@ComponentScan("com.dhc.country")  //自动扫描指定包下的所有组件
@MapperScan("com.dhc.country.mapper")
@EnableHystrix //熔断器
@SpringBootApplication(scanBasePackages="controller")
public class CountryApplication {
    public static void main(String[] args) {
        SpringApplication.run(CountryApplication.class, args);
    }
}
