package com.dhc.teacher.service;

import com.dhc.order.dto.OrderDTO;
import com.dhc.teacher.dto.TeacherDTO;
import com.dhc.teacher.entity.Teacher;
import com.dhc.order.api.IOrderApi;
import com.dhc.order.dto.OrderDTO;

import java.util.List;

public interface TeacherService {

    List<Teacher> listTeacher();

    Teacher getTeacher(Integer id);

    Boolean saveTeacher(TeacherDTO teacherDTO);

    Boolean updateTeacher(TeacherDTO teacherDTO);

    Boolean deleteTeacher(Integer id);

    Boolean insertTeacherList(List<TeacherDTO> teacherDTO);

    Boolean checkTeacherExist(String id);

    OrderDTO getOrderInfo(Integer id);

    int getAvgScore(String id);

}
