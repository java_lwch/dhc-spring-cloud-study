package com.dhc.studentcourse.dto;

import lombok.Data;

@Data
public class CourseDto {
    private Integer courseId;
    private String courseName;
}

