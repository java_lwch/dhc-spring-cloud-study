package com.dhc.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.dorm.entity.Dorm;
import com.dhc.dorm.entity.Student;
import com.dhc.dorm.entity.Teacher;
import com.dhc.dorm.entity.Student;

import java.util.List;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/11 23:00
 */
public interface DormMapper extends BaseMapper<Dorm> {
    List<Dorm> selectDormList();
    Teacher selectTeacherWithStudent(String course_name);
    List<Student> selectAvgFromDrom(int dorm_id);
}
