package com.dhc.zhufeng.dto;


import lombok.Data;

@Data
public class StudentDTO {

    private Integer no;

    private String name;

    private Integer math;

    private Integer chinese;

    private Integer english;

    private Integer sum;
}
