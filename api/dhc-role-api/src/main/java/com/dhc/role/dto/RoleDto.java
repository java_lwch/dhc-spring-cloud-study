package com.dhc.role.dto;

import lombok.Data;

@Data
public class RoleDto {
    private Integer RoleId;

    private String RoleName;
}
