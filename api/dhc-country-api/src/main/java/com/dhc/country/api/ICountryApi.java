package com.dhc.country.api;

import com.dhc.country.dto.CountryDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "dhc-country-svc", path = "/country")
public interface ICountryApi {
        /**
         * 根据id 查询国家
         * @param id 国家id
         * @return 国家信息
         */
        @GetMapping("/{id}")
        CountryDTO getCountry(@PathVariable("id")Integer id);

}
