package com.dhc.user_role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author sunzhs
 */
@EqualsAndHashCode(callSuper = true)
@TableName("t_user")
@Data
public class User extends Model<User> {
    //用户id
    @TableId("user_id")
    private Integer userId;

    //用户名字
    @TableField("user_name")
    private String userName;

    //用户年龄
    @TableField("user_age")
    private Integer userAge;

    //邮件地址
    @TableField("email_address")
    private String emailAddress;
}
