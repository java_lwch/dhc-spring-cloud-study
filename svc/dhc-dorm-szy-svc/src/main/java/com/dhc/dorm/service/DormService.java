package com.dhc.dorm.service;

import com.dhc.dorm.dto.DormDTO;
import com.dhc.dorm.entity.Dorm;
import com.dhc.dorm.entity.Teacher;
import com.dhc.dorm.entity.Student;

import java.util.List;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/11 22:59
 */
public interface DormService {
    List<Dorm> listDorm();

    Dorm getDorm(Integer dorm_id);

    Boolean saveDorm(DormDTO dormDTO);

    Boolean updateDorm(DormDTO dormDTO);

    Boolean deleteDorm(int dorm_id);

    Teacher selectTeacherWithStudentWithTheHighestScoreInSpecifiedCourse(String course_name);

    List<Student> selectAvgFromDrom(int dorm_id);
}
