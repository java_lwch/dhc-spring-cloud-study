package com.dhc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.demo.entity.Product;

/**
 * @author liwencheng
 */
public interface ProductMapper extends BaseMapper<Product> {
}
