package com.dhc.user_role;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author sunzhs
 */
@SpringCloudApplication
@EnableFeignClients("com.dhc")
@MapperScan("com.dhc.user_role.mapper")
public class UserRoleApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserRoleApplication.class,args);
    }
}
