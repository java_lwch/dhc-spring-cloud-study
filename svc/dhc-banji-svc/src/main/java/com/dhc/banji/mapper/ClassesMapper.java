package com.dhc.banji.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.banji.entity.ClassesEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ClassesMapper extends BaseMapper<ClassesEntity> {
        List<ClassesEntity> selectClassesList();
        List<ClassesEntity> selectById(Integer classesId);
        Integer getMaxClassesOfSchool();
        Double selectAVGScoreByTeacher(int teacher_id);
        String getStudentNameLower60(int class_id);
}
