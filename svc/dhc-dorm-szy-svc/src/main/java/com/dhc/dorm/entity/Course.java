package com.dhc.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/13 0:50
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_course")
public class Course extends Model<Course> {
    @TableId(value = "course_id", type = IdType.AUTO)
    private int courseid;

    @TableField(value = "course_no")
    private int courseno;

    @TableField(value = "course_name")
    private String coursename;

    @TableField(value = "course_score")
    private String coursescore;
}
