package com.dhc.studentcourse;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringCloudApplication
@EnableFeignClients("com.dhc")
public class StudentCourseApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentCourseApplication.class, args);
    }
}
