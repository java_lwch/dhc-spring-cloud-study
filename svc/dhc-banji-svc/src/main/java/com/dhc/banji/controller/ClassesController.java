package com.dhc.banji.controller;

import com.dhc.banji.api.IClassesApi;
import com.dhc.banji.entity.ClassesEntity;
import com.dhc.banji.service.ClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/classes")
@RefreshScope
public class ClassesController {
    @Autowired
    private ClassesService classesService;

    @Autowired
    private IClassesApi iClassesApi;

    @GetMapping("/{schoolId}")
    public Integer getMaxClassesOfSchool(@PathVariable("schoolId")Integer schoolId){
        return classesService.getMaxClassesOfSchool(schoolId);
    }


    @GetMapping()
    public List<ClassesEntity> listClasses(){
        return classesService.listClasses();
    }

    @GetMapping("/{classesId}")
    public List<ClassesEntity> getClasses(@PathVariable("classesId")Integer classesId){
        return classesService.getClasses(classesId);
    }

    @DeleteMapping("/{classesId}")
    public Boolean deleteClasses(@PathVariable("classesId")Integer classesId){
        return classesService.deleteClasses(classesId);
    }

    @PutMapping
    public Boolean updateClasses(@RequestBody ClassesEntity classesEntity){
        return classesService.updateClasses(classesEntity);
    }

    @PostMapping
    public Boolean saveClasses(@RequestBody ClassesEntity classesEntity){
        return classesService.saveClasses(classesEntity);
    }

    @PostMapping("/list")
    public Boolean insertClassesList(@RequestBody List<ClassesEntity> classesEntity){
        return classesService.insertClassesList(classesEntity);
    }

    @GetMapping("/getAvgScore/{teacher_id}")
    public Double selectAVGScoreByTeacher(@PathVariable("teacher_id")int teacher_id){
        return classesService.selectAVGScoreByTeacher(teacher_id);
    }

    @GetMapping("/getStudentName/{class_id}")
    public String getStudentNameLower60(@PathVariable("class_id")int class_id){
        return classesService.getStudentNameLower60(class_id);
    }

}
