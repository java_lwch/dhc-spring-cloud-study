package com.dhc.studentcourse.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("course")
public class CourseEntity extends Model<CourseEntity> {

    @TableId(value = "course_id")
    private Integer courseId;

    @TableField(value = "course_name")
    private String courseName;

}
