CREATE TABLE `course` (
	`student_id` INT(3) NOT NULL,
	`course_id` INT(4) NOT NULL,
	`course_name` VARBINARY(50) NULL DEFAULT NULL,
	`school_no` CHAR(1) NULL DEFAULT NULL,
	`student_class` VARBINARY(50) NULL DEFAULT NULL,
	`course_achievement` INT(20) NOT NULL,
	PRIMARY KEY (`course_id`, `student_id`)
)
