package com.dhc.score.controller;


import com.dhc.score.api.IScoreApi;
import com.dhc.score.dto.ScoreDTO;
import com.dhc.score.entity.Score;
import com.dhc.score.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author dongjianmin
 */
@RestController
@RequestMapping("/score")
@RefreshScope
public class ScoreController {


    @Autowired
    private ScoreService scoreService;

    /**
     * 根据学生sid，课程cid 查询成绩
     * @param sid 学生id，cid 课程id
     * @return 分数
     */

    @GetMapping("/{ScoreSid}/{ScoreCid}")
    public  Score selectScoreById(@PathVariable("ScoreSid")Integer ScoreSid, @PathVariable("ScoreCid")Integer ScoreCid){
        return scoreService.selectScoreById(ScoreSid,ScoreCid);
    }


    /**
     * 根据学生id 查询成绩
     *
     * @param sid 学生id
     * @return 分数的集合
     */
    @GetMapping("/{ScoreSid}")
    public  List<Score> selectScoreBySid(@PathVariable("ScoreSid")Integer ScoreSid){
        return scoreService.selectScoreBySid(ScoreSid);
    }
}
