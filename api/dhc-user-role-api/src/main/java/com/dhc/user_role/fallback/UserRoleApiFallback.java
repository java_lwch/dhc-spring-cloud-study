package com.dhc.user_role.fallback;

import com.dhc.user_role.api.IUserRoleApi;
import com.dhc.user_role.dto.UserRoleDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class UserRoleApiFallback implements IUserRoleApi {

    @Override
    public List<UserRoleDTO> findAllUserRole() {
        List<UserRoleDTO> userRoleDTOS = new ArrayList<>();
        UserRoleDTO userRoleDTO1 = new UserRoleDTO();
        userRoleDTO1.setUserName("张三");
        userRoleDTO1.setUserAge(20);
        userRoleDTO1.setEmailAddress("zhangsan@666.com");
        userRoleDTO1.setRoleName("普通用户");
        userRoleDTO1.setAddTime(new Date());
        userRoleDTO1.setUpdateTime(new Date());
        userRoleDTOS.add(userRoleDTO1);
        return userRoleDTOS;
    }

    @Override
    public List<UserRoleDTO> findRolesByUserId(Integer userId) {
        List<UserRoleDTO> userRoleDTOS = new ArrayList<>();
        UserRoleDTO userRoleDTO1 = new UserRoleDTO();
        userRoleDTO1.setUserName("李四");
        userRoleDTO1.setUserAge(23);
        userRoleDTO1.setEmailAddress("lisi@666.com");
        userRoleDTO1.setRoleName("高级用户");
        userRoleDTO1.setAddTime(new Date());
        userRoleDTO1.setUpdateTime(new Date());
        userRoleDTOS.add(userRoleDTO1);
        return userRoleDTOS;
    }

    @Override
    public List<UserRoleDTO> findUsersByRoleId(Integer roleId) {
        List<UserRoleDTO> userRoleDTOS = new ArrayList<>();
        UserRoleDTO userRoleDTO1 = new UserRoleDTO();
        userRoleDTO1.setUserName("王五");
        userRoleDTO1.setUserAge(30);
        userRoleDTO1.setEmailAddress("wangwu@666.com");
        userRoleDTO1.setRoleName("管理员");
        userRoleDTO1.setAddTime(new Date());
        userRoleDTO1.setUpdateTime(new Date());
        userRoleDTOS.add(userRoleDTO1);
        return userRoleDTOS;
    }

    @Override
    public int insertUserRole(Integer userId, Integer roleId) {
        return 666;
    }

    @Override
    public int updateUserRole(Integer userId, Integer roleId) {
        return 666;
    }

    @Override
    public int deleteUserRole(Integer userId, Integer roleId) {
        return 666;
    }

    @Override
    public int deleteRolesByUserId(Integer userId) {
        return 666;
    }

    @Override
    public int deleteUsersByRoleId(Integer roleId) {
        return 666;
    }
}
