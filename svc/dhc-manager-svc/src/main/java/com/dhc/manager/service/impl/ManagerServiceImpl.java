package com.dhc.manager.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.manager.dto.ManagerDTO;
import com.dhc.manager.entity.Manager;
import com.dhc.manager.mapper.ManagerMapper;
import com.dhc.manager.service.ManagerService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ManagerServiceImpl extends ServiceImpl<ManagerMapper, Manager> implements ManagerService {

    @Autowired
    ManagerMapper managerMapper;

    @Override
    public List<Manager> selectAll(){
        return managerMapper.selectManagerAll();
    }

    @Override
    public Boolean deleteManager(Integer managerId){
        return super.removeById(managerId);
    }

    @Override
    public Boolean updateManager(ManagerDTO managerDTO) {

        Manager m=new Manager();
        return m.update(new UpdateWrapper<Manager>()
                .set("manager_name", managerDTO.getManagerName())
                .eq("manager_id", managerDTO.getManagerId()));
    }

    @Override
    public Boolean saveManager(ManagerDTO managerDTO){
        Manager m=new Manager();
        BeanUtils.copyProperties(managerDTO, m);
        return m.insert();
    }


}