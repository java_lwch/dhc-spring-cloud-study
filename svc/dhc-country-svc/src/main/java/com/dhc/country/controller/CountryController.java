package com.dhc.country.controller;

import com.dhc.country.dto.CountryDTO;
import com.dhc.country.entity.Country;
import com.dhc.country.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/country")
@RefreshScope
public class CountryController {


    @Autowired
    private CountryService countryService;

    /**
     * 查询所有国家的信息
     * @return 所有国家的结果集合
     */
    @GetMapping("/select")
    public List<Country> listCountry(){
        return countryService.listCountry();
    }

    /**
     * 添加国家的全部信息
     * @param countryDTO 国家的信息
     * @return 是否更新成功
     */
    @PostMapping
    public Boolean saveCountry(@RequestBody CountryDTO countryDTO){
        return countryService.saveCountry(countryDTO);
    }

    /**
     * 更新国家的全部信息
     * @param countryDTO 国家的信息
     * @return 是否更新成功
     */
    @PutMapping
    public Boolean updateCountry(@RequestBody CountryDTO countryDTO){
        return countryService.updateCountry(countryDTO);
    }

    /**
     * 通过国家id进行删除
     * @param CountryId 国家id
     * @return 是否删除成功
     */
    @DeleteMapping("/Delete/{CountryId}")
    public Boolean deleteCountry(@PathVariable("CountryId")Integer CountryId){
        return countryService.deleteCountry(CountryId);
    }

    /**
     * 根据国家id进行查询数据
     * @param CountryId 国家Id
     * @return 查询结果的集合
     */
    @GetMapping(value = "/{CountryId}",produces = "appliction/json;charset=utf-8")
    public Country selectCountryById(@PathVariable("CountryId")Integer CountryId){
        return countryService.selectCountryById(CountryId);
    }

    /**
     * 通过国家名字进行模糊查询
     * @param CountryName 国家的名字
     * @return 查询结果的集合
     */
    @RequestMapping("/SelectByName/{CountryName}")
    public List<Country> listCountryByName(@PathVariable("CountryName")String CountryName){
        return countryService.listCountryByName(CountryName);
    }
}
