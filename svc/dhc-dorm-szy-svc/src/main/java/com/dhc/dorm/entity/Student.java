package com.dhc.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/22 16:23
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_student")
public class Student extends Model<Student> {
    @TableId(value = "student_id", type = IdType.AUTO)
    private int studentid;

    @TableField(value = "student_no")
    private int studentno;

    @TableField(value = "student_name")
    private String studentname;

    @TableField(value = "avgScore")
    private double avgScore;
}
