package com.dhc.dormitory.api;

import com.dhc.dormitory.dto.DormitoryDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author wangyijun
 */
@FeignClient(name = "dhc-dormitory-svc", path = "/dormitory")
public interface IDormitoryApi {

    /**
     * 查询所有宿舍信息
     * @return 宿舍结果集
     */
    @GetMapping("/queryAll")
    List<DormitoryDTO> getAllDorm();

    /**
     * 通过宿舍ID查询宿舍信息
     * @param 宿舍ID
     * @return 宿舍结果
     */
    @GetMapping("/query/{dormId}")
    DormitoryDTO getDormByID(@PathVariable("dormId")String dormitoryId);

    /**
     * 插入1条新的宿舍记录
     * @return 更新是否成功
     */
    @PostMapping
    Boolean addDorm(@RequestBody DormitoryDTO dormDTO);

    /**
     * 删除1条宿舍记录
     * @return 更新是否成功
     */
    @DeleteMapping("delete/{dormId}")
    Boolean delDorm(@PathVariable("dormId")String dormitoryId);

    /**
     * 更新1条宿舍记录
     * @return 更新是否成功
     */
    @PostMapping
    Boolean updDorm(@RequestBody DormitoryDTO dormDTO);

}
