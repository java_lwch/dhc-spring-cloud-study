package com.dhc.school.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.dhc.school.dto.SchoolDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author HanwenZhang
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_school")
public class School extends Model<School> {
    //学校id
    @TableId(value = "School_id", type = IdType.AUTO)
    private  Integer SchoolId;

    //学校姓名
    @TableField("School_name")
    private  String SchoolName;

    //学校类型
    @TableField("School_type")
    private String SchoolType;

    //学校地址
    @TableField("School_address")
    private String SchoolAddress;

    //学校邮箱
    @TableField("School_Email")
    private String SchoolEmail;

    //城市id
    @TableField("city_id")
    private  Integer cityId;

}