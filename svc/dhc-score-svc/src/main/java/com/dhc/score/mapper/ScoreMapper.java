package com.dhc.score.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.score.entity.Score;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import java.util.List;
/**
 * @author dongjm
 * @version 1.0
 */

@Mapper
@Repository
public interface ScoreMapper extends BaseMapper<Score> {

    /**
     * 根据学生id 查询成绩
     * @param sid 学生id
     * @return 分数的集合
     */
    List<Score> selectScoreBySid(Integer ScoreSid);

    /**
     * 根据学生sid，课程id 查询成绩
     * @param sid 学生id，cid 课程id
     * @return 分数
     */
    Score selectScoreById(Integer ScoreSid,Integer ScoreCid);

}