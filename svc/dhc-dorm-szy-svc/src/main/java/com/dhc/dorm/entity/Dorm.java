package com.dhc.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/11 23:01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_dorm")
public class Dorm extends Model<Dorm> {
    @TableId(value = "dorm_id", type = IdType.AUTO)
    private int dormid;

    @TableField(value = "dorm_no")
    private int dormno;

    @TableField(value = "dorm_name")
    private String dormname;
}
