package com.dhc.student.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.student.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StudentMapper extends BaseMapper<Student> {

    List<Student> selectStudentList();
    List<Student> selectStudentByNameList(String student_name);
}
