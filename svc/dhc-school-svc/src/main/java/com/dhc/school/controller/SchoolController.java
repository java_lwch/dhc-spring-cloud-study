package com.dhc.school.controller;


import com.dhc.school.api.ISchoolApi;
import com.dhc.school.dto.SchoolDTO;
import com.dhc.school.entity.School;
import com.dhc.school.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author HanwenZhang
 * @version 1.0
 */

@RestController
@RequestMapping("/school")
@RefreshScope
public class SchoolController {

    @Autowired
    private SchoolService schoolService;

    /**
     * 查询所有学校的信息
     * @return 所有学校的结果集合
     */
    @GetMapping("/select")
    public List<School> listSchool(){
        return schoolService.listSchool();
    }

    /**
     * 添加学校的全部信息
     * @param schoolDTO
     * @return 是否更新成功
     */
    @PostMapping
    public Boolean saveSchool(@RequestBody SchoolDTO schoolDTO){
        return schoolService.saveSchool(schoolDTO);
    }

    /**
     * 更新学校的全部信息
     * @param schoolDTO
     * @return 是否更新成功
     */
    @PutMapping
    public Boolean updateSchool(@RequestBody SchoolDTO schoolDTO){
        return schoolService.updateSchool(schoolDTO);
    }

    /**
     * 通过学校id进行删除
     * @param SchoolId 学校id
     * @return 是否删除成功
     */
    @DeleteMapping("/{SchoolId}")
    public Boolean deleteSchool(@PathVariable("SchoolId")Integer SchoolId){
        return schoolService.deleteSchool(SchoolId);
    }

    /**
     * 根据学校id进行查询数据
     * @param SchoolId 学校Id
     * @return 查询结果的集合
     */
    @GetMapping("/{SchoolId}")
    public  School selectSchoolById(@PathVariable("SchoolId")Integer SchoolId){
        return schoolService.selectSchoolById(SchoolId);
    }

    /**
     * 通过学校名字进行模糊查询
     * @param SchoolName 学校的名字
     * @return 查询结果的集合
     */
    @RequestMapping("/SelectByName/{SchoolName}")
    public List<School> listSchoolByName(@PathVariable("SchoolName")String SchoolName){
        return schoolService.listSchoolByName(SchoolName);
    }
}