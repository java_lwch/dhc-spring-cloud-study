package com.dhc.city.dto;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.print.attribute.standard.Chromaticity;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Data
public class CityDTO {

    private Integer CityId;
    private String CityName;
    private String CityType;
    private String CityAddress;
    private String CityEmail;
}
