package com.dhc.course.api;

import com.dhc.course.dto.AchievementDTO;
import com.dhc.course.dto.CourseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "dhc-course-svc" , path = "/course")
public interface ICourseApi {

    /**
     * 根据id 查询课程
     * @param id 课程id
     * @return 课程信息
     */
    @GetMapping("/{id}")
    CourseDTO getCourse(@PathVariable("id")Integer id);

    /**
     * 根据课程id 查询指定课程在60分以上的人数
     * @param id 课程id
     * @return 人数信息
     */
    @GetMapping("/{id}")
    AchievementDTO getNumberOfStudents(@PathVariable("id")Integer id);

}
