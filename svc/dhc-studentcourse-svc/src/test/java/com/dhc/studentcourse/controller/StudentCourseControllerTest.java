package com.dhc.studentcourse.controller;

import com.alibaba.fastjson.JSON;
import com.dhc.studentcourse.entity.StudentCourseEntity;
import com.dhc.studentcourse.service.StudentCourseService;
import javafx.beans.value.ObservableBooleanValue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest()
public class StudentCourseControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

//    @MockBean
//    @Qualifier("studentCourse")
//    private StudentCourseService studentCourseService;

    @Before
    public void init() throws IOException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void destroy() {

    }

    @Test
    public void testGetStudentCourseList() throws Exception {
        mockMvc.perform(get(
                "/studentcourse/101")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSON.toJSONString(null)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isNotEmpty());
    }
}