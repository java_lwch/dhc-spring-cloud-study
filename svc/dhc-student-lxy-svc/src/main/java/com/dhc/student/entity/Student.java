package com.dhc.student.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_student")
public class Student extends Model<Student> {
    @TableId(value = "student_id",type = IdType.AUTO)
    int studentId;

    @TableField(value = "student_name")
    String studentName;

    @TableField(value = "student_gender")
    String studentGender;

    @TableField(value = "student_class")
    int studentClass;

    @TableField(value= "student_dorm")
    int studentDorm;

    @TableField(value = "city_name")
    String cityName;

    @TableField(value = "student_point")
    double studentPoint;

}