package com.dhc.teacher.dto;

import lombok.Data;

@Data
public class TeacherDTO {
    private Integer id;
    private String  teacherNo;
    private String  teacherName;
    private String  teacherSex;
    private Integer teacherAge;
    private Integer teacherSalary;
    private String  schoolNo;

}
