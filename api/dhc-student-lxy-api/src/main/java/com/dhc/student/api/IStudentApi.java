package com.dhc.student.api;

import com.dhc.student.dto.StudentDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "dhc-student-svc", path = "/student")
public interface IStudentApi {

    @GetMapping("/selectStudentName/{studentName}")
    public Double selectAvgPoint(@PathVariable("studentName")String studentName);
}
