package com.dhc.dormitory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.dormitory.entity.Dormitory;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author wangyijun
 */
@Mapper
@Repository
public interface DormitoryMapper extends BaseMapper<Dormitory> {

    /**
     * 查询所有宿舍信息
     * @return 宿舍结果集
     */
    List<Dormitory> getAllDorm();

    /**
     * 通过宿舍ID查询宿舍信息
     * @return 宿舍结果
     */
    Dormitory getDormByID(String dormitoryId);

}
