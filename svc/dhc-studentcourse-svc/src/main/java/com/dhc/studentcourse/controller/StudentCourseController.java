package com.dhc.studentcourse.controller;

import com.dhc.studentcourse.entity.StudentCourseEntity;
import com.dhc.studentcourse.service.StudentCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/studentcourse")
//@RefreshScope
public class StudentCourseController {

    @Autowired
    private StudentCourseService studentCourseService;

    @GetMapping()
    public List<StudentCourseEntity> listStudentCourse(){
        return studentCourseService.listStudentCourse();
    }

    @PostMapping
    public Boolean insertStudentCourse(@RequestBody StudentCourseEntity studentCourseEntity){
        return studentCourseService.insertStudentCourse(studentCourseEntity);
    }

    /**
     * 根据学生id，查询学生课程信息
     *
     * @param studentId 学生id
     * @return 学生课程信息
     */
    @GetMapping("/{studentId}")
    public List<StudentCourseEntity> getStudentCourseList(@PathVariable("studentId")Integer studentId){
        return studentCourseService.getStudentCourseList(studentId);
    }

    @PutMapping
    public Boolean updateStudentCourse(@RequestBody StudentCourseEntity studentCourseEntity){
    return studentCourseService.updateStudentCourse(studentCourseEntity);
}
    /**
     * 根据学生id，删除学生课程信息
     *
     * @param studentId 学生id
     * @return ture/false
     */
    @DeleteMapping("/{studentId}")
    public Boolean deleteOrder(@PathVariable("studentId")Integer studentId){
        return studentCourseService.deleteStudentCourse(studentId);
    }

    /**
     * 根据学生id，课程id ，新课程名，更新学生课程信息
     * check后，更新
     * @param studentId 学生id ，courseId 课程id
     * @return true/false
     */
    @GetMapping("/{studentId}/{courseId}/{courseName}/")
    public Boolean setStudentCourse(@PathVariable("studentId")Integer studentId,@PathVariable("courseId")Integer courseId,@PathVariable("courseName")String courseName){
        return studentCourseService.updateStudent(studentId,courseId,courseName);
    }

    /**
     * 根据学生id，课程id 查询学生信息，课程信息
     * check后，登录
     * @param studentId 学生id ，courseId 课程id
     * @return true/false
     */
    @GetMapping("/{studentId}/{courseId}")
    public Boolean insertStudent(@PathVariable("studentId")Integer studentId,@PathVariable("courseId")Integer courseId){
        return studentCourseService.insertStudent(studentId,courseId);
    }

}
