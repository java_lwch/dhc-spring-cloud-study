package com.dhc.user_role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author sunzhs
 */
@EqualsAndHashCode(callSuper = true)
@TableName("t_role")
@Data
public class Role extends Model<Role> {
    //角色id
    @TableId("role_id")
    private Integer roleId;

    //角色名字
    @TableField("role_name")
    private String roleName;
}
