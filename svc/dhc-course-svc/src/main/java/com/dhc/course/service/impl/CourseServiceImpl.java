package com.dhc.course.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.course.entity.NumberOfStudentsEntity;
import com.dhc.course.mapper.NumberOfStudentsMapper;
import com.dhc.course.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl extends ServiceImpl<NumberOfStudentsMapper, NumberOfStudentsEntity> implements CourseService {

    @Autowired
    private NumberOfStudentsMapper numberOfStudentsMapper;

    @Override
    public NumberOfStudentsEntity getNumberOfStudents(Integer courseId) {
        return numberOfStudentsMapper.selectNumberOfStudents(courseId);
    }



}
