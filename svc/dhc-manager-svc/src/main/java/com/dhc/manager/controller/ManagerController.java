package com.dhc.manager.controller;
import com.dhc.manager.dto.ManagerDTO;
import com.dhc.manager.entity.Manager;
import com.dhc.manager.service.impl.ManagerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RefreshScope
@RequestMapping("/manager")
public class ManagerController {

    @Autowired
    ManagerServiceImpl managerService;

    @GetMapping("/select")
    public List<Manager> selectAll(){
        return managerService.selectAll();
    }

    @DeleteMapping("/{managerId}")
    public Boolean deleteManager(@PathVariable("managerId")Integer managerId){
        return managerService.deleteManager(managerId);
    }

    @PutMapping
    public Boolean updateManager(@RequestBody ManagerDTO managerDTO){
        return managerService.updateManager(managerDTO);
    }

    @PostMapping
    public Boolean saveManager(@RequestBody ManagerDTO managerDTO){
        return managerService.saveManager(managerDTO);
    }

}