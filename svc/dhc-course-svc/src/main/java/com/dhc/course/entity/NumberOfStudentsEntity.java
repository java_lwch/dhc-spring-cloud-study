package com.dhc.course.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("course")
public class NumberOfStudentsEntity extends Model<NumberOfStudentsEntity> {

    @TableId(value = "course_id")
    private Integer courseId;

    @TableField(value = "number_of_students")
    private Integer numberOfStudents;

}
