package com.dhc.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author liwencheng
 */
@EqualsAndHashCode(callSuper = true)
@TableName("t_product")
@Data
public class Product extends Model<Product> {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("product_no")
    private String productNo;

    @TableField("product_name")
    private String productName;

    @TableField("price")
    private BigDecimal price;
}
