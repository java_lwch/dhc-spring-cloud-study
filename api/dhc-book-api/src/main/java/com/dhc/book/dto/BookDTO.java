package com.dhc.book.dto;

import lombok.Data;

@Data
public class BookDTO {
    Integer bookId;
    String bookName;
    String bookWriter;
}
