package com.dhc.dormitory.service;

import com.dhc.dormitory.entity.Dormitory;
import com.dhc.dormitory.dto.DormitoryDTO;

import java.util.List;

/**
 * @author wangyijun
 */
public interface DormitoryService {

    /**
     * 查询所有宿舍信息
     * @return 宿舍结果集
     */
    List<Dormitory> getAllDorm();

    /**
     * 通过宿舍ID查询宿舍信息，如果宿舍ID为空则返回全部宿舍信息
     * @return 宿舍结果集
     */
    Dormitory getDormByID(String dormitoryId);

    /**
     * 插入1条新的宿舍记录
     * @return 更新是否成功
     */
    Boolean addDorm(DormitoryDTO dormDTO);

    /**
     * 删除1条宿舍记录
     * @return 更新是否成功
     */
    Boolean delDorm(String dormitoryId);

    /**
     * 更新1条宿舍记录
     * @return 更新是否成功
     */
    Boolean updDorm(DormitoryDTO dormDTO);
}
