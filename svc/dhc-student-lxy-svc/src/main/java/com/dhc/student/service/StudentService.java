package com.dhc.student.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.student.entity.Student;
import com.dhc.student.mapper.StudentMapper;
import org.springframework.stereotype.Service;

import java.util.List;

public interface StudentService{

    public Double selectAvgPoint(String studentName);
}
