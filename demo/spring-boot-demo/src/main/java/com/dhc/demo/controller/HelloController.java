package com.dhc.demo.controller;

import com.dhc.demo.components.RedisUtil;
import com.dhc.demo.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * @author liwencheng
 */
@Controller
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    @Qualifier("hello1")
    private HelloService hello1Service;

    @Resource(name = "hello2")
    private HelloService hello2Service;

    @Autowired
    private RedisUtil redisUtil;


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ResponseBody
    public String test(){
        return hello1Service.test();
    }

    @RequestMapping(value = "/test2", method = RequestMethod.GET)
    @ResponseBody
    public String test2(){
        return hello2Service.test();
    }

    @GetMapping(value = "/setRedisKey")
    @ResponseBody
    public Boolean setRedisKey(){
        redisUtil.set("hello:hello", "hello redis");
        return true;
    }

    @GetMapping(value = "/getRedisKey")
    @ResponseBody
    public String getRedisKey(){
        redisUtil.expire("hello", 30);
        return (String)redisUtil.get("hello");
    }
}
