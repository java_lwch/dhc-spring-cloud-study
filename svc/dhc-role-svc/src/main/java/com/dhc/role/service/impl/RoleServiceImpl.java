package com.dhc.role.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.role.entity.Role;
import com.dhc.role.mapper.RoleMapper;
import com.dhc.role.service.RoleService;
import com.dhc.role.dto.RoleDto;
import org.springframework.beans.factory.annotation.Autowired;

public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;
    /**
     * 添加角色的全部信息
     * @param roleDto
     * @return 是否更新成功
     */
    @Override
    public Boolean saveRole(RoleDto roleDto) {
        return true;
    }

    /**
     * 更新角色的全部信息
     * @param roleDto
     * @return 是否更新成功
     */
    @Override
    public Boolean updateRole(RoleDto roleDto) {
        return true;
    }

    /**
     * 通过角色id进行删除
     * @param RoleId 角色id
     * @return 是否删除成功
     */
    @Override
    public Boolean deleteRole(Integer RoleId) {
        return true;
    }

    /**
     * 根据角色id进行查询数据
     * @param RoleId 角色Id
     * @return 查询结果的集合
     */
    @Override
    public Role selectRoleById(Integer RoleId){
        return roleMapper.selectRoleById(RoleId);
    }
}
