package com.dhc.product.api;

import com.dhc.product.dto.ProductDTO;
import com.dhc.product.fallback.HystrixFallbackFactory;
import com.dhc.product.fallback.IProductApiFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author liwencheng
 */
@FeignClient(name = "dhc-product-svc", path = "/product",
        fallbackFactory = HystrixFallbackFactory.class)
public interface IProductApi {

    /**
     * 根据id 查询商品
     * @param id 商品id
     * @return 商品信息
     */
    @GetMapping("/{id}")
    ProductDTO getProduct(@PathVariable("id")Integer id);

}
