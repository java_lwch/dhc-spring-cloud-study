package com.dhc.course.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.course.entity.CourseEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CourseMapper extends BaseMapper<CourseEntity> {
    List<CourseEntity> selectCourseList();
    List<CourseEntity> selectById(Integer courseId);
}
