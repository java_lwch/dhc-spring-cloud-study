package com.dhc.demo.components;

import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * @author liwencheng
 */
@RestControllerAdvice
public class ControllerValidAdvice {

    /**
     * Post请求参数拦截
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public String bindExceptionHandler(MethodArgumentNotValidException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        List<ObjectError> errors = bindingResult.getAllErrors();
        for (ObjectError error : errors) {
            return error.getDefaultMessage();
        }
        return "";
    }

    /**
     * Get请求参数拦截
     */
    @ExceptionHandler(BindException.class)
    public String bindExceptionHandler(BindException ex) {
        List<ObjectError> errors = ex.getBindingResult().getAllErrors();
        for (ObjectError error : errors) {
            return error.getDefaultMessage();
        }
        return "";
    }
}
