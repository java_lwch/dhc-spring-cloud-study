package com.dhc.banji.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_classes")
public class ClassesEntity extends Model<ClassesEntity> {

    @TableId(value = "classes_id")
    private Integer classesId;

    @TableField(value = "classes_name")
    private String  classesName;

    @TableField(value = "school_id")
    private Integer schoolId;

    public Integer getClassesId() {
        return classesId;
    }

    public void setClassesId(Integer classesId) {
        this.classesId = classesId;
    }

    public String getClassesName() {
        return classesName;
    }

    public void setClassesName(String classesName) {
        this.classesName = classesName;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public ClassesEntity() {
    }
}
