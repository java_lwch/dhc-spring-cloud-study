package com.dhc.studentcourse.service;

import com.dhc.studentcourse.entity.StudentCourseEntity;

import java.util.List;

public interface StudentCourseService {
    List<StudentCourseEntity> listStudentCourse();

    StudentCourseEntity getStudentCourse(Integer studentId);

    List<StudentCourseEntity> getStudentCourseList(Integer studentId);

    Boolean insertStudentCourse(StudentCourseEntity studentCourseEntity);

    Boolean updateStudentCourse(StudentCourseEntity studentCourseEntity);

    Boolean updateStudent(Integer studentId,Integer courseId,String courseName);

    Boolean insertStudent(Integer studentId,Integer courseId);

    Boolean deleteStudentCourse(Integer studentId);
}
