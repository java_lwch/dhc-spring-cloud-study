package com.dhc.manager.mapper;

import com.dhc.manager.entity.Manager;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
@Repository
public interface ManagerMapper extends BaseMapper<Manager>{
    List<Manager> selectManagerAll();
}