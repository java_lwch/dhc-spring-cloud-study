package com.dhc.user_role.service;

import com.dhc.user_role.entity.Role;
import org.springframework.stereotype.Service;

/**
 * @author sunzhs
 */

public interface RoleService {
    /**
     * 根据角色id获取角色信息
     * @param id 角色id
     * @return 指定id的角色信息
     */
    Role getRoleById(Integer id);
}
