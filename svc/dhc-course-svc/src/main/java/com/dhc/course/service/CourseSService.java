package com.dhc.course.service;

import com.dhc.course.entity.CourseEntity;

import java.util.List;

public interface CourseSService {

    List<CourseEntity> listCourse() ;
    List<CourseEntity> getCourse(Integer courseId);

    Boolean saveCourse(CourseEntity courseEntity);

    Boolean updateCourse(CourseEntity courseEntity);

    Boolean deleteCourse(Integer courseId);

    Boolean insertCourseList(List<CourseEntity> courseEntity);
}
