package com.dhc.dorm.controller;

import com.dhc.course.api.ICourseApi;
import com.dhc.course.dto.CourseDTO;
import com.dhc.dorm.dto.DormDTO;
import com.dhc.dorm.entity.Dorm;
import com.dhc.dorm.entity.Teacher;
import com.dhc.dorm.service.DormService;
import com.dhc.dorm.entity.Student;
import com.dhc.student.api.IStudentApi;
import com.dhc.student.dto.StudentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/10 23:28
 */
@RestController
@RequestMapping("/dorm")
@RefreshScope
public class DormController {
    @Autowired
    private DormService dormService;

    @Autowired
    private IStudentApi studentApi;

    @Autowired
    private ICourseApi courseApi;

    /**
     * 根据id 查询宿舍
     *
     * @param dorm_id  宿舍ID
     * @return 宿舍信息
     */
    @GetMapping("/{dorm_id}")
    public DormDTO getDorm(@PathVariable("dorm_id") Integer dorm_id) {
        DormDTO dormDTO = new DormDTO();
        dormDTO.setDorm_id(1);
        dormDTO.setDorm_no(2);
        dormDTO.setDorm_name("setDorm_name3333");
        return dormDTO;
    }

    @GetMapping("/select")
    public List<Dorm> selectDormList(){
        return dormService.listDorm();
    }

    @GetMapping("select/{dorm_id}")
    public Dorm getOrder(@PathVariable("dorm_id")Integer dorm_id){
        return dormService.getDorm(dorm_id);
    }

    @PostMapping
    public Boolean saveDorm(@RequestBody DormDTO dormDTO){
        return dormService.saveDorm(dormDTO);
    }

    @PutMapping
    public Boolean updateDorm(@RequestBody DormDTO dormDTO){
        return dormService.updateDorm(dormDTO);
    }

    @DeleteMapping("/{dorm_id}")
    public Boolean deleteDorm(@PathVariable("dorm_id")Integer dorm_id){
        return dormService.deleteDorm(dorm_id);
    }

    @GetMapping("/getTeacher/{course_name}")
    public Teacher selectTeacherWithStudentWithTheHighestScoreInSpecifiedCourse(@PathVariable("course_name")String course_name){
        return dormService.selectTeacherWithStudentWithTheHighestScoreInSpecifiedCourse(course_name);
    }

    @GetMapping("/selectAvgFromDrom/{dorm_id}")
    public List<CourseDTO> selectAvgFromDrom(@PathVariable("dorm_id")int dorm_id) {
        List<StudentDTO> studentDTOList = studentApi.getStudentByDormId(dorm_id);
        List<CourseDTO> avgScoreList = null;
        if (studentDTOList != null && studentDTOList.size() != 0) {
            List<String> studentIdList = new ArrayList<>();
            studentDTOList.forEach(s -> studentIdList.add(String.valueOf(s.getStudentid())));
            avgScoreList  = courseApi.getAvgScoreByStudentIDList(studentIdList);
        }
        return avgScoreList;
    }
}
