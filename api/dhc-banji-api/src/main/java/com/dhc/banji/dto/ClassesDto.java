package com.dhc.banji.dto;

import lombok.Data;

@Data
public class ClassesDto {
   private Integer classesId;
   private String  classesName;
   private Integer schoolId;
}
