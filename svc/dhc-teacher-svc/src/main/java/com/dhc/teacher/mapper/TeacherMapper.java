package com.dhc.teacher.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.teacher.entity.Teacher;

import java.util.List;

public interface                                                                                                                                                                  TeacherMapper extends BaseMapper<Teacher> {
    List<Teacher> selectTeacherList();
    List<Teacher> selectTeacherNo(String teacherNo);
    int selectAvgScore(String teacherNo);
}
