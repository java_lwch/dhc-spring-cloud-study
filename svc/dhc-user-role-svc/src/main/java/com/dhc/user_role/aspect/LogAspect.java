package com.dhc.user_role.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Aspect
@Component
public class LogAspect {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 切入点
     */
    @Pointcut("execution(* com.dhc.user_role.controller.*.*(..))")
    public void log(){}

    /**
     * 前置通知
     */
    @Before("log()")
    public void doBefore(){
        log.info("---------来请求了---------");
    }

    /**
     * 最终通知
     */
    @After("log()")
    public void doAfter(JoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String url = request.getRequestURL().toString();
        String ip = request.getRemoteAddr();
        String classMethod = joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        RequestMethod requestLog = new RequestMethod(url, ip, classMethod, args);
        log.info("Request : {}", requestLog);
    }


    private class RequestMethod {
        private String url;         // 请求 url
        private String ip;          // 请求 ip
        private String classMethod; // 请求方法
        private Object[] args;      // 请求参数

        public RequestMethod(String url, String ip, String classMethod, Object[] args) {
            this.url = url;
            this.ip = ip;
            this.classMethod = classMethod;
            this.args = args;
        }

        @Override
        public String toString() {
            return "RequestMethod{" +
                    "url='" + url + '\'' +
                    ", ip='" + ip + '\'' +
                    ", classMethod='" + classMethod + '\'' +
                    ", args=" + Arrays.toString(args) +
                    '}';
        }
    }
}
