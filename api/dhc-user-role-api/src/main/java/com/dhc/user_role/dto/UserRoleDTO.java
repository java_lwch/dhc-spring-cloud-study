package com.dhc.user_role.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author sunzhs
 */
@Data
public class UserRoleDTO{
    //用户名字
    private String userName;

    //角色名
    private String roleName;

    //更新时间
    private Date updateTime;

    //添加时间
    private Date addTime;

    //用户年龄
    private Integer userAge;

    //用户邮件地址
    private String emailAddress;
}
