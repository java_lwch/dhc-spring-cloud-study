package com.dhc.demo.components;

import com.dhc.demo.dto.OrderDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author liwencheng
 */
@Component
public class MyComponent {

    @Bean
    public OrderDTO getMyOrderDTO(){
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setOrderNo("AAA");
        orderDTO.setOrderName("BBB");
        return orderDTO;
    }
}
