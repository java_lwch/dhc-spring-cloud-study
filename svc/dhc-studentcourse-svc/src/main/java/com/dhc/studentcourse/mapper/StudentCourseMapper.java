package com.dhc.studentcourse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.studentcourse.entity.StudentCourseEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StudentCourseMapper extends BaseMapper<StudentCourseEntity> {
    List<StudentCourseEntity> selectStudentCourseList();

    List<StudentCourseEntity> selectStudentCourseList1(Integer studentId);

    StudentCourseEntity selectStudentCourse(Integer studentId,Integer courseId);

    Boolean updateStudent(Integer studentId,Integer courseId,String courseName);

    Boolean insertStudent(Integer studentId,Integer courseId,String studentName, String studentSex,String studentClass,String courseName);
}
