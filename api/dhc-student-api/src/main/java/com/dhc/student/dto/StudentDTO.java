package com.dhc.student.dto;

import lombok.Data;

@Data
public class StudentDTO {

    private Integer classId;
    private Integer sid;
    private String name;
    private Integer score;
}
