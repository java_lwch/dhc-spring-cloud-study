package com.dhc.dormitory.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wangyijun
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_dormitory")
public class Dormitory extends Model<Dormitory> {

    //宿舍ID唯一
    @TableId(value = "dormitory_id", type = IdType.INPUT)
    private String dormitoryId;

    //宿舍名称（汉字）
    @TableField("dormitory_name")
    private String dormitoryName;

    //宿舍所属学校ID
    @TableField("school_id")
    private String schoolId;

}
