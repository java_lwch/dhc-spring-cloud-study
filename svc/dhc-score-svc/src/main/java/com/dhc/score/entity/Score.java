package com.dhc.score.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.dhc.score.dto.ScoreDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author dongjm
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_score")
public class Score extends Model<Score> {
    //学生sid
    @TableId(value = "Score_sid", type = IdType.AUTO)
    private  Integer ScoreSid;

    //课程cid
    @TableField("Score_cid")
    private  String ScoreCid;

    //考试分数
    @TableField("Score_scores")
    private String ScoreScores;

}