package com.dhc.student.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.student.entity.Student;

import java.util.List;

public interface StudentMapper extends BaseMapper<Student> {
    public Double selectStudentPoint(String studentName);
}
