package com.dhc.banji.api;

import com.dhc.banji.dto.ClassesDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "dhc-banji-svc", path = "/classes")
public interface IClassesApi {

    /**
     * 根据班级id 查询班级信息
     * @param  classesId
     * @return 返回班级信息
     */

    @GetMapping("/{classesId}")
    List<ClassesDto> getClassesList(@PathVariable("classesId")Integer classesId);

    /**
     * 根据学校id 查询指定学校学生总分平均成绩最高的班级
     * @param  schoolId
     * @return 返回学生总分平均成绩最高的班级id
     */

    @GetMapping("/{schoolId}")
    Integer getMaxClassesOfSchool(@PathVariable("schoolId")Integer schoolId);

    @GetMapping("/getAvgScore/{teacher_id}")
    public Double selectAVGScoreByTeacher(@PathVariable("teacher_id")int teacher_id);

    @GetMapping("/getStudentName/{class_id}")
    public String getStudentNameLower60(@PathVariable("class_id")int class_id);
}
