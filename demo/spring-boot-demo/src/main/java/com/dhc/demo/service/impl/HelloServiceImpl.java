package com.dhc.demo.service.impl;

import com.dhc.demo.service.HelloService;
import org.springframework.stereotype.Service;

/**
 * @author liwencheng
 */
@Service("hello1")
public class HelloServiceImpl implements HelloService {


    @Override
    public String test(){
        return "this is service hello 1";
    }
}
