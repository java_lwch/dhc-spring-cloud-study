package com.dhc.dormitory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.dormitory.dto.DormitoryDTO;
import com.dhc.dormitory.entity.Dormitory;
import com.dhc.dormitory.mapper.DormitoryMapper;
import com.dhc.dormitory.service.DormitoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wangyijun
 */
@Service
public class DormitoryServiceImpl extends ServiceImpl<DormitoryMapper, Dormitory> implements DormitoryService {

    @Autowired
    private DormitoryMapper dormitoryMapper;

    /**
     * 查询所有宿舍信息
     * @return 宿舍结果集
     */
    @Override
    public List<Dormitory> getAllDorm() {
        return dormitoryMapper.getAllDorm();
    }

    /**
     * 通过宿舍ID查询宿舍信息
     * @param 宿舍ID
     * @return 宿舍结果
     */
    @Override
    public Dormitory getDormByID(String dormitoryId){
        return dormitoryMapper.getDormByID(dormitoryId);
    }

    /**
     * 插入1条新的宿舍记录
     * @return 更新是否成功
     */
    @Override
    public Boolean addDorm(DormitoryDTO dormDTO) {
        Dormitory dormitory = new Dormitory();
        BeanUtils.copyProperties(dormDTO, dormitory);
        return dormitory.insert();
    }

    /**
     * 删除1条宿舍记录
     * @return 更新是否成功
     */
    @Override
    public Boolean delDorm(String dormitoryId){
        return super.removeById(dormitoryId);
    }

    /**
     * 更新1条宿舍记录
     * @return 更新是否成功
     */
    @Override
    public Boolean updDorm(DormitoryDTO dormDTO){
        if (dormDTO!=null && dormDTO.getDormitoryId()!=null){
            Dormitory dorm = new Dormitory();
            dorm.setDormitoryId(dormDTO.getDormitoryId());
            dorm.setDormitoryName(dormDTO.getDormitoryName());
            dorm.setSchoolId(dormDTO.getSchoolId());
            return super.updateById(dorm);
        }
        return false;
    }
}
