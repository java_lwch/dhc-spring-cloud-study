package com.dhc.studentcourse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.studentcourse.entity.CourseEntity;
import com.dhc.studentcourse.entity.StudentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface StudentMapper extends BaseMapper<StudentEntity> {
    CourseEntity selectStudent(Integer studentId);
}
