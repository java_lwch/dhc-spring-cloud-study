package com.dhc.studentcourse.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("student_info")
public class StudentEntity extends Model<StudentEntity> {

    @TableId(value = "student_id")
    private Integer studentId;

    @TableField(value = "student_name")
    private String studentName;

    @TableField(value = "student_sex")
    private String studentSex;

    @TableField(value = "student_class")
    private String studentClass;

}
