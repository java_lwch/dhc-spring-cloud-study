package com.dhc.dormitory.dto;

import lombok.Data;

/**
 * @author wangyijun
 */
@Data
public class DormitoryDTO {

    //宿舍ID唯一
    private String dormitoryId;

    //宿舍名称（汉字）
    private String dormitoryName;

    //宿舍所属学校ID
    private String schoolId;
}
