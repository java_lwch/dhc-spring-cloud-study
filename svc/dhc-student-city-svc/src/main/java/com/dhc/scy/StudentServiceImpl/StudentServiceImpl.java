package com.dhc.scy.StudentServiceImpl;

import com.dhc.scy.entity.Student;
import com.dhc.scy.mapper.StudentMapper;
import com.dhc.scy.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentMapper studentMapper;

    @Override
    public Student selectStudentCity(Integer no) {

        return studentMapper.selectStudentCity(no);
    }
}
