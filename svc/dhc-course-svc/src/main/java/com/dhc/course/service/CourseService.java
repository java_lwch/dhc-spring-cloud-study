package com.dhc.course.service;

import com.dhc.course.entity.NumberOfStudentsEntity;
import com.dhc.course.entity.CourseEntity;
import java.util.List;

public interface CourseService {


    NumberOfStudentsEntity getNumberOfStudents(Integer studentId);



}
