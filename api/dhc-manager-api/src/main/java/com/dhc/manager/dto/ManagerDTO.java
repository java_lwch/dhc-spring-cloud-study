package com.dhc.manager.dto;

import lombok.Data;

@Data
public class ManagerDTO {
    private Integer managerId;
    private String managerName;
    private Integer userId;

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }
}
