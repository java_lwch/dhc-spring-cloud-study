package com.dhc.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author liwencheng
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_order")
public class Order extends Model<Order> {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "order_no")
    private String orderNo;


    @TableField(value = "order_name")
    private String orderName;
}
