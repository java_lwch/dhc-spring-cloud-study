package com.dhc.teacher.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.teacher.entity.Teacher;
import com.dhc.teacher.mapper.TeacherMapper;
import com.dhc.teacher.service.TeacherService;
import com.dhc.teacher.dto.TeacherDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeanUtils;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.transaction.annotation.Transactional;
import com.dhc.order.api.IOrderApi;
import com.dhc.order.dto.OrderDTO;
import com.dhc.school.api.ISchoolApi;
import com.dhc.school.dto.SchoolDTO;

import javax.annotation.Resource;
import java.util.List;
import java.util.ArrayList;
import java.lang.Integer;
import java.lang.String;

@Service("teacher1")
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;

    @Autowired
    private TeacherServiceImpl teacherService;

    @Override
    public List<Teacher> listTeacher() {
        return teacherMapper.selectTeacherList();
    }

    @Override
    public Teacher getTeacher(Integer id) {
        return teacherMapper.selectById(id);
    }

    @Override
    public Boolean saveTeacher(TeacherDTO teacherDTO) {
        Teacher teacher = new Teacher();
        BeanUtils.copyProperties(teacherDTO, teacher);
        if (checkSchoolExist(teacher.getSchoolNo()).equals(false) || checkTeacherRepeat(teacher.getTeacherNo()).equals(false)){
            return false;
        }
        return teacher.insert();
    }

    @Override
    public Boolean updateTeacher(TeacherDTO teacherDTO) {
        Teacher teacherCheck = new Teacher();
        BeanUtils.copyProperties(teacherDTO, teacherCheck);

        if (checkSchoolExist(teacherCheck.getSchoolNo()).equals(false)){
            return false;
        }

        Teacher teacher = new Teacher();
        return teacher.update(new UpdateWrapper<Teacher>()
                .set("teacher_name", teacherDTO.getTeacherName())
                .set("teacher_sex", teacherDTO.getTeacherSex())
                .set("teacher_age", teacherDTO.getTeacherAge())
                .set("teacher_salary", teacherDTO.getTeacherSalary())
                .set("school_no", teacherDTO.getSchoolNo())
                .eq("teacher_no", teacherDTO.getTeacherNo()));

    }

    @Override
    public Boolean deleteTeacher(Integer id) {
        return super.removeById(id);
    }

    @Override
    public Boolean insertTeacherList(List<TeacherDTO> teacherDTOS) {
        for(TeacherDTO t:teacherDTOS){
            Teacher teacher = new Teacher();
            BeanUtils.copyProperties(t, teacher);
            if (checkSchoolExist(t.getSchoolNo()).equals(false) || checkTeacherRepeat(t.getTeacherNo()).equals(false)){
                return false;
            }
        }
        for(TeacherDTO t:teacherDTOS){
            Teacher teacher = new Teacher();
            BeanUtils.copyProperties(t, teacher);
            teacher.insert();
        }
        return true;
    }

    //对外提供接口：校验教师存不存在，传入要查询的教师卡号，如果存在返回ture，不存在返回false。
    @Override
    public Boolean checkTeacherExist(String teacherNo) {
        String tt = teacherMapper.selectTeacherNo(teacherNo).toString();
        if ( tt.equals("[]")){
            return false;
        } else{
            return true;}
    }



    //接口测试
    @Autowired
    IOrderApi IOrderApi;

    @Override
    public OrderDTO getOrderInfo(Integer id) {
        return IOrderApi.getOrder(id);
    }



    //老师表进行插入或更新操作时判断该卡号是否存在，避免教师卡号重复
    public Boolean checkTeacherRepeat(String teacherNo) {
        if ( checkTeacherExist(teacherNo ) ){
            return false;
        } else{
            return true;
        }
    }


    @Autowired
    ISchoolApi ISchoolApi;

    //从外部接口校验学校是否存在，存在则可以进行插入或更新操作，不存在则不能操作。
    public Boolean checkSchoolExist(String schoolNo) {
        return ISchoolApi.IsSchoolIdExist(Integer.parseInt(schoolNo));
    }

    @Override
    public int getAvgScore(String teacherNo){
        return teacherMapper.selectAvgScore(teacherNo);
    }
}
