package com.dhc.scy.api;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "dhc-student-city-svc",path = "/scy")
public interface IStudentCityApi {

    @GetMapping("/{no}")
    String getStudentCity(Integer no);
}
