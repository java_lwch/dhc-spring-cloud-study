package com.dhc.course.dto;

import lombok.Data;

@Data
public class CourseDTO {

    private String courseId;
    private String courseName;
    private Integer schoolId;



}
