package com.dhc.user_role.controller;

import com.dhc.user_role.dto.UserRoleDTO;
import com.dhc.user_role.service.UserRoleService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author sunzhs
 */

@RestController
@RequestMapping("/userRole")
@RefreshScope
@Data
public class UserRoleController {

    @Autowired
    UserRoleService userRoleService;
    /**
     * 查询所有的用户角色关系记录
     * @return 用户角色关系List
     */
    @GetMapping("/getAllUserRoleInfo")
    public List<UserRoleDTO> findAllUserRole(){
        return userRoleService.findAllUserRole();
    }

    /**
     * 查询指定用户的所有角色
     * @param userId 用户id
     * @return 用户角色关系
     */
    @GetMapping("/getRolesInfoByUserId/{userId}")
    public List<UserRoleDTO> findRolesByUserId(@PathVariable("userId") Integer userId){
        return userRoleService.findRolesByUserId(userId);
    }

    /**
     * 查询指定角色下的所有用户
     * @param roleId 角色id
     * @return 用户角色关系
     */
    @GetMapping("/getUsersInfoByRoleId/{roleId}")
    public List<UserRoleDTO> findUsersByRoleId(@PathVariable("roleId") Integer roleId){
        return userRoleService.findUsersByRoleId(roleId);
    }

    /**
     * 添加用户和角色关系
     * @param userId 新增的用户id
     * @param roleId 新增的角色id
     * @return 添加成功的件数
     */
    @PostMapping("/addNewUserRole/{userId}/{roleId}")
    public int insertUserRole(@PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId){
        return userRoleService.insertUserRole(userId,roleId);
    }

    /**
     * 更新用户角色关系
     * @param userId 希望更新的用户Id
     * @param roleId 希望更新的角色Id
     * @return 更新成功的记录条数
     */
    @PutMapping("/update/{userId}/{roleId}")
    public int updateUserRole(@PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId){
        return userRoleService.updateUserRole(userId,roleId);
    }

    /**
     * 删除用户角色关系
     * @param userId 希望删除的用户Id
     * @param roleId 希望删除的角色Id
     * @return 删除成功的记录条数
     */
    @DeleteMapping("/deleteUniqueUserRole/{userId}/{roleId}")
    public int deleteUserRole(@PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId){
        return userRoleService.deleteUserRole(userId,roleId);
    }

    /**
     * 删除指定用户的全部角色
     * @param userId 希望删除的用户Id
     * @return 删除成功的记录条数
     */
    @DeleteMapping("/deleteRolesByUserId/{userId}")
    public int deleteRolesByUserId(@PathVariable("userId") Integer userId){
        return userRoleService.deleteRolesByUserId(userId);
    }

    /**
     * 删除指定角色下的所有用户
     * @param roleId 希望删除的角色Id
     * @return 删除成功的记录条数
     */
    @DeleteMapping("/deleteUsersByRoleId/{roleId}")
    public int deleteUsersByRoleId(@PathVariable("roleId") Integer roleId){
        return userRoleService.deleteUsersByRoleId(roleId);
    }

}
