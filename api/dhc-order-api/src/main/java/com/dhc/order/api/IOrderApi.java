package com.dhc.order.api;

import com.dhc.order.dto.OrderDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author liwencheng
 */
@FeignClient(name = "dhc-order-svc", path = "/order")
public interface IOrderApi {

    /**
     * 根据id 查询订单
     * @param id 订单id
     * @return 订单信息
     */
    @GetMapping("/{id}")
    OrderDTO getOrder(@PathVariable("id")Integer id);

}
