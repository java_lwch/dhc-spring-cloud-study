package com.dhc.user_role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.user_role.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author sunzhs
 */
@Mapper
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {
    /**
     * 查询所有的用户角色关系记录
     * @return 用户角色关系List
     */
    List<UserRole> findAllUserRole();

    /**
     * 查询指定用户的所有角色
     * @param userId 用户id
     * @return 用户角色关系
     */
    List<UserRole> findRolesByUserId(@PathVariable("userId") Integer userId);

    /**
     * 查询指定角色下的所有用户
     * @param roleId 角色id
     * @return 用户角色关系
     */
    List<UserRole> findUsersByRoleId(@PathVariable("roleId") Integer roleId);

    /**
     * 添加用户和角色关系
     * @param userId 新增的用户id
     * @param roleId 新增的角色id
     * @return 添加成功的件数
     */
    int insertUserRole(@PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId);

    /**
     * 更新用户角色关系
     * @param userId 希望更新的用户Id
     * @param roleId 希望更新的角色Id
     * @return 更新成功的记录条数
     */
    int updateUserRole(@PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId);

    /**
     * 删除用户角色关系
     * @param userId 希望删除的用户Id
     * @param roleId 希望删除的角色Id
     * @return 删除成功的记录条数
     */
    int deleteUserRole(@PathVariable("userId") Integer userId, @PathVariable("roleId") Integer roleId);

    /**
     * 删除指定用户的全部角色
     * @param userId 希望删除的用户Id
     * @return 删除成功的记录条数
     */
    int deleteRolesByUserId(@PathVariable("userId") Integer userId);

    /**
     * 删除指定角色下的所有用户
     * @param roleId 希望删除的角色Id
     * @return 删除成功的记录条数
     */
    int deleteUsersByRoleId(@PathVariable("roleId") Integer roleId);
}
