package com.dhc.zhufeng.controller;


import com.dhc.zhufeng.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/zf")
@RefreshScope
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/getSum")
    String getCha()
    {
        return "当前最大最小分数差为"+studentService.selectCha();
    }
}
