package com.dhc.studentcourse.api;


import com.dhc.studentcourse.dto.StudentCourseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "dhc-studentcourse-svc", path = "/studentcourse")
public interface IStudentCourseApi {

    /**
     * 根据学生id 查询学生信息
     * @param studentId
     * @return 订学生信息
     */

    @GetMapping("/{studentId}")
    List<StudentCourseDto> getStudentCourseList(@PathVariable("studentId")Integer studentId);

    /**
     * 根据学生id，删除学生课程信息
     *
     * @param studentId 学生id
     * @return ture/false
     */
    @DeleteMapping("/{studentId}")
    Boolean deleteOrder(@PathVariable("studentId")Integer studentId);

    /**
     * 根据学生id，课程id ，新课程名，更新学生课程信息
     * check后，更新
     * @param studentId 学生id ，courseId 课程id
     * @return true/false
     */
    @GetMapping("/{studentId}/{courseId}/{courseName}/")
    Boolean setStudentCourse(@PathVariable("studentId")Integer studentId,@PathVariable("courseId")Integer courseId,@PathVariable("courseName")String courseName);

    /**
     * 根据学生id，课程id 查询学生信息，课程信息
     * check后，登录
     * @param studentId 学生id ，courseId 课程id
     * @return true/false
     */
    @GetMapping("/{studentId}/{courseId}")
    public Boolean insertStudent(@PathVariable("studentId")Integer studentId,@PathVariable("courseId")Integer courseId);
}
