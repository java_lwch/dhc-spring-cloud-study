package com.dhc.book.service;

import com.dhc.book.entity.Book;
import com.dhc.book.dto.BookDTO;

import java.util.List;

public interface BookService {

     List<Book> selectAll();

   Boolean saveBook(BookDTO bookDTO);

   Boolean deleteBook(Integer bookId);

   Boolean updateBook(BookDTO bookDTO);


}


