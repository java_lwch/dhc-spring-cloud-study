package com.dhc.studentcourse.dto;

import lombok.Data;

@Data
public class StudentDto {
    private Integer studentId;
    private String studentName;
    private String studentSex;
    private String studentClass;
}
