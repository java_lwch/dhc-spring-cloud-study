package com.dhc.user_role.service;

import com.dhc.user_role.dto.UserRoleDTO;
import com.dhc.user_role.entity.Role;
import com.dhc.user_role.entity.User;
import com.dhc.user_role.entity.UserRole;
import com.dhc.user_role.mapper.UserRoleMapper;
import lombok.Data;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sunzhs
 */
@Service
@Data
public class UserRoleServiceImp implements UserRoleService{

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    /**
     * 获取全部的用户关系数据
     * @return 用户关系list
     */
    @Override
    public List<UserRoleDTO> findAllUserRole() {
        //获取全部用户和角色的对应关系
        List<UserRole> userRoles = userRoleMapper.findAllUserRole();
        //创建UserRoleDTO的List用于保存查询结果
        List<UserRoleDTO> userRoleDTOS = new ArrayList<UserRoleDTO>();
        //遍历用户角色关系
        for(UserRole ur : userRoles){
            //创建UserRoleDTO实例用于保存查询结果
            UserRoleDTO urd = new UserRoleDTO();
            //调用UserService中的查询,查询对应id的用户信息
            User user = userService.getUserById(ur.getUserId());
            //调用RoleService中的查询,查询对应id的角色信息
            Role role = roleService.getRoleById(ur.getRoleId());

            //设定用户名
            urd.setUserName(user.getUserName());
            //设定角色名
            urd.setRoleName(role.getRoleName());
            //设定用户年龄
            urd.setUserAge(user.getUserAge());
            //设定用户邮件地址
            urd.setEmailAddress(user.getEmailAddress());
            //设定添加时间
            urd.setAddTime(ur.getAddTime());
            //设定更新时间
            urd.setUpdateTime(ur.getUpdateTime());

            //添加到List中
            userRoleDTOS.add(urd);
        }
        return userRoleDTOS;
    }

    /**
     * 根据用户Id获得该用户的所有角色
     * @param userId 用户id
     * @return 指定用户的全部角色
     */
    @Transactional
    @Override
    public List<UserRoleDTO> findRolesByUserId(Integer userId) {
        //根据用户Id获得该用户的所有角色
        List<UserRole> userRoles = userRoleMapper.findRolesByUserId(userId);
        //创建UserRoleDTO的List用于保存查询结果
        List<UserRoleDTO> userRoleDTOS = new ArrayList<UserRoleDTO>();
        //遍历用户角色关系
        for(UserRole ur : userRoles){
            //创建UserRoleDTO实例用于保存查询结果
            UserRoleDTO urd = new UserRoleDTO();
            //调用UserService中的查询,查询对应id的用户信息
            User user = userService.getUserById(ur.getUserId());
            //调用RoleService中的查询,查询对应id的角色信息
            Role role = roleService.getRoleById(ur.getRoleId());

            //设定用户名
            urd.setUserName(user.getUserName());
            //设定角色名
            urd.setRoleName(role.getRoleName());
            //设定用户年龄
            urd.setUserAge(user.getUserAge());
            //设定用户邮件地址
            urd.setEmailAddress(user.getEmailAddress());
            //设定添加时间
            urd.setAddTime(ur.getAddTime());
            //设定更新时间
            urd.setUpdateTime(ur.getUpdateTime());

            //添加到List中
            userRoleDTOS.add(urd);
        }
        return userRoleDTOS;
    }

    /**
     * 根据角色Id获得，符合该角色的所有用户
     * @param roleId 角色id
     * @return 指定角色的全部用户
     */
    @Transactional
    @Override
    public List<UserRoleDTO> findUsersByRoleId(Integer roleId) {
        //根据角色Id获得，符合该角色的所有用户
        List<UserRole> userRoles = userRoleMapper.findUsersByRoleId(roleId);
        //创建UserRoleDTO的List用于保存查询结果
        List<UserRoleDTO> userRoleDTOS = new ArrayList<UserRoleDTO>();
        for(UserRole ur : userRoles){
            //创建UserRoleDTO实例用于保存查询结果
            UserRoleDTO urd = new UserRoleDTO();
            //调用UserService中的查询,查询对应id的用户信息
            User user = userService.getUserById(ur.getUserId());
            //调用RoleService中的查询,查询对应id的角色信息
            Role role = roleService.getRoleById(ur.getRoleId());

            //设定用户名
            urd.setUserName(user.getUserName());
            //设定角色名
            urd.setRoleName(role.getRoleName());
            //设定用户年龄
            urd.setUserAge(user.getUserAge());
            //设定用户邮件地址
            urd.setEmailAddress(user.getEmailAddress());
            //设定添加时间
            urd.setAddTime(ur.getAddTime());
            //设定更新时间
            urd.setUpdateTime(ur.getUpdateTime());

            //添加到List中
            userRoleDTOS.add(urd);
        }
        return userRoleDTOS;
    }

    /**
     * 添加新的用户角色关系
     * @param userId 新增的用户id
     * @param roleId 新增的角色id
     * @return 添加成功的记录数
     */
    @Transactional
    @Override
    public int insertUserRole(Integer userId, Integer roleId) {
        return userRoleMapper.insertUserRole(userId,roleId);
    }

    /**
     * 更新已经存在的用户角色关系
     * @param userId 希望更新的用户Id
     * @param roleId 希望更新的角色Id
     * @return 更新成功的记录数
     */
    @Transactional
    @Override
    public int updateUserRole(Integer userId, Integer roleId) {
        return userRoleMapper.updateUserRole(userId,roleId);
    }

    /**
     * 删除指定用户的指定角色
     * @param userId 希望删除的用户Id
     * @param roleId 希望删除的角色Id
     * @return 删除成功的记录条数
     */
    @Transactional
    @Override
    public int deleteUserRole(Integer userId, Integer roleId) {
        return userRoleMapper.deleteUserRole(userId,roleId);
    }

    /**
     * 删除指定用户的所有角色
     * @param userId 希望删除的用户Id
     * @return 删除成功的记录数
     */
    @Transactional
    @Override
    public int deleteRolesByUserId(Integer userId) {
        return userRoleMapper.deleteRolesByUserId(userId);
    }

    /**
     * 删除指定角色下的所用用户
     * @param roleId 希望删除的角色Id
     * @return 删除成功的记录数
     */
    @Transactional
    @Override
    public int deleteUsersByRoleId(Integer roleId) {
        return userRoleMapper.deleteUsersByRoleId(roleId);
    }
}
