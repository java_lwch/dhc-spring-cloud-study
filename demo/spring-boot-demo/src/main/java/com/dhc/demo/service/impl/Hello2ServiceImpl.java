package com.dhc.demo.service.impl;

import com.dhc.demo.controller.HelloController;
import com.dhc.demo.service.HelloService;
import org.springframework.stereotype.Service;

/**
 * @author liwencheng
 */
@Service("hello2")
public class Hello2ServiceImpl implements HelloService {

    @Override
    public String test(){


        return "this is service hello 2";
    }
}
