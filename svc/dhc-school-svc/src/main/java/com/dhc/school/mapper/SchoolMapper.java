package com.dhc.school.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.school.entity.School;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import java.util.List;
/**
 * @author HanwenZhang
 * @version 1.0
 */

@Mapper
@Repository
public interface SchoolMapper extends BaseMapper<School> {

    /**
     * 查询所有学校的信息
     * @return 所有学校的结果集合
     */
    List<School> selectSchoolList();

    /**
     * 通过学校名字进行模糊查询
     * @param SchoolName 学校的名字
     * @return 查询结果的集合
     */
    List<School> getSchoolByName(String SchoolName);

    /**
     * 根据学校id进行查询数据
     * @param SchoolId 学校Id
     * @return 查询结果的集合
     */
    School selectSchoolById(Integer SchoolId);

    School selectId();
}