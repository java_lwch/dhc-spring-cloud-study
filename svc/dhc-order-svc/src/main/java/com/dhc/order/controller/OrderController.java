package com.dhc.order.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.dhc.order.dto.OrderDTO;
import com.dhc.product.api.IProductApi;
import com.dhc.product.dto.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liwencheng
 */
@RestController
@RequestMapping("/order")
@RefreshScope
public class OrderController {

    @Value("${server.port}")
    public Integer port;

    @Value("${order.name}")
    private String orderName;

    @Autowired
    private IProductApi productApi;

    /**
     * 根据id 查询订单
     *
     * @param id 订单id
     * @return 订单信息
     */
    @GetMapping("/{id}")
    public OrderDTO getOrder(@PathVariable("id") Integer id) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(1);
        orderDTO.setOrderNo(String.valueOf(port));
        orderDTO.setOrderName(orderName);
        ProductDTO productDTO = productApi.getProduct(id);
        orderDTO.setProductNo(productDTO.getProductNo());
        orderDTO.setProductName(productDTO.getProductName());
        return orderDTO;
    }
}
