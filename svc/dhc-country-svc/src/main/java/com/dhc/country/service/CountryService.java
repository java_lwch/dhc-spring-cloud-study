package com.dhc.country.service;

import com.dhc.country.dto.CountryDTO;
import com.dhc.country.entity.Country;
import org.springframework.stereotype.Service;

import java.util.List;
//@Service
public interface CountryService {

    /**
     * 查询所有国家的信息
     * @return 所有国家的结果集合
     */
    List<Country> listCountry();

    /**
     * 添加国家的全部信息
     * @param countryDTO 国家的信息
     * @return 是否更新成功
     */
    Boolean saveCountry(CountryDTO countryDTO);

    /**
     * 更新国家的全部信息
     * @param countryDTO 国家的信息
     * @return 是否更新成功
     */
    Boolean updateCountry(CountryDTO countryDTO);

    /**
     * 通过国家id进行删除
     * @param countryId 国家id
     * @return 是否删除成功
     */
    Boolean deleteCountry(Integer countryId);

    /**
     * 根据国家id进行查询数据
     * @param CountryId 国家Id
     * @return 查询结果的集合
     */
    Country selectCountryById(Integer CountryId);

    /**
     * 通过国家名字进行模糊查询
     * @param CountryName 国家的名字
     * @return 查询结果的集合
     */
    List<Country> listCountryByName(String CountryName);
}
