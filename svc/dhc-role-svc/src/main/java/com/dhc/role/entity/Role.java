package com.dhc.role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;



@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_role")
public class Role extends Model<Role> {

    @TableId(value = "Role_id")
    private Integer RoleId;

    @TableField(value = "Role_name")
    private String RoleName;
}
