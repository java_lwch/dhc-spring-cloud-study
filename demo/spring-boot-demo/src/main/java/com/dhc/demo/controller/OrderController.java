package com.dhc.demo.controller;

import com.dhc.demo.components.MyConfiguration;
import com.dhc.demo.dto.OrderDTO;
import com.dhc.demo.entity.Order;
import com.dhc.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author liwencheng
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping()
    public List<Order> listOrder(){
        return orderService.listOrder();
    }

    @GetMapping("/{orderId}")
    public Order getOrder(@PathVariable("orderId")Integer orderId){
        return orderService.getOrder(orderId);
    }

    @PostMapping
    public Boolean saveOrder(@RequestBody OrderDTO orderDTO){
        return orderService.saveOrder(orderDTO);
    }

    @PutMapping
    public Boolean updateOrder(@RequestBody OrderDTO orderDTO){
        return orderService.updateOrder(orderDTO);
    }

    @DeleteMapping("/{orderId}")
    public Boolean deleteOrder(@PathVariable("orderId")Integer orderId){
        return orderService.deleteOrder(orderId);
    }

    @PostMapping("/list")
    public Boolean insertOrderList(@RequestBody List<OrderDTO> orderDTO){
        return orderService.insertOrderList(orderDTO);
    }
}
