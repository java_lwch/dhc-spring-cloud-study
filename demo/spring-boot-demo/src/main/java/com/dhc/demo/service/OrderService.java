package com.dhc.demo.service;

import com.dhc.demo.dto.OrderDTO;
import com.dhc.demo.entity.Order;

import java.util.List;

/**
 * @author liwencheng
 */
public interface OrderService {
    List<Order> listOrder();

    Order getOrder(Integer orderId);

    Boolean saveOrder(OrderDTO orderDTO);

    Boolean updateOrder(OrderDTO orderDTO);

    Boolean deleteOrder(Integer orderId);

    Boolean insertOrderList(List<OrderDTO> orderDTO);
}
