package com.dhc.user_role.mapper;

import com.baomidou.mybatisplus.core.mapper.Mapper;
import com.dhc.user_role.entity.Role;
import org.springframework.stereotype.Repository;

/**
 * @author sunzhs
 */

@Repository
public interface RoleMapper extends Mapper <Role>{

    /**
     * 根据角色id获取角色信息
     * @param id 角色id
     * @return 指定id的角色信息
     */
    Role getRoleById(Integer id);

}
