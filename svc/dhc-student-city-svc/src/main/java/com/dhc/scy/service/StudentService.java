package com.dhc.scy.service;

import com.dhc.scy.entity.Student;

import java.util.List;

public interface StudentService {

    //查询学生所在的城市
    Student selectStudentCity(Integer no);
}
