package com.dhc.book.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.book.dto.BookDTO;
import com.dhc.book.entity.Book;
import com.dhc.book.mapper.BookMapper;
import com.dhc.book.service.BookService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book>implements BookService {

    @Autowired
    BookMapper bookMapper;

    @Override
    public List<Book> selectAll(){
        return  bookMapper.selectSchoolList();
    }

    @Override
    public  Boolean saveBook(BookDTO bookDTO){
        Book book = new Book();
        BeanUtils.copyProperties(bookDTO, book);
        return book.insert();
    }

    @Override
    public Boolean deleteBook(Integer bookId) {
        return super.removeById(bookId);
    }

    @Override
    public Boolean updateBook(BookDTO bookDTO){
        Book book=new Book();
        return book.update(new UpdateWrapper<Book>()
                .set("book_name", bookDTO.getBookName())
                .set("book_writer", bookDTO.getBookWriter())
                .eq("book_id", bookDTO.getBookId()));
    }
}