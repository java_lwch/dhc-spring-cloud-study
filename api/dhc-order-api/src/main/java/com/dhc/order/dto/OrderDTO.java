package com.dhc.order.dto;

import lombok.Data;

/**
 * @author liwencheng
 */
@Data
public class OrderDTO {

    private Integer id;
    private String orderNo;
    private String orderName;
    private String productNo;
    private String productName;
}
