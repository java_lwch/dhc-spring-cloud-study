package com.dhc.user_role.mapper;

import com.baomidou.mybatisplus.core.mapper.Mapper;
import com.dhc.user_role.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author sunzhs
 */

@Repository
public interface UserMapper extends Mapper<User> {

    /**
     * 根据指定的用户id
     * @param id 用户id
     * @return 指定用户id的用户信息
     */
    User getUserById(Integer id);

}
