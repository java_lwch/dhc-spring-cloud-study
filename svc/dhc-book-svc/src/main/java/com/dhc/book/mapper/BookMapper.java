package com.dhc.book.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.book.entity.Book;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BookMapper extends BaseMapper<Book> {
     List<Book> selectSchoolList();
}
