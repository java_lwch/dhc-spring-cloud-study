package com.dhc.demo.service.impl;

import com.dhc.demo.entity.Product;
import com.dhc.demo.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * @author liwencheng
 */
@Service
public class ProductServiceImpl {

    @Autowired
    private ProductMapper productMapper;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Boolean save() {
        Product product = new Product();
        product.setProductNo("aaa");
        product.setProductName("aaa");
        product.setPrice(BigDecimal.ONE);
        product.insert();
        int j = 1 / 0;
        return true;
    }


}
