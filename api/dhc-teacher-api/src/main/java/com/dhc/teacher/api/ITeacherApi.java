package com.dhc.teacher.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.dhc.teacher.dto.TeacherDTO;

import java.util.List;

@FeignClient(name = "dhc-teacher-svc", path = "/teacher")
public interface ITeacherApi {

    @GetMapping("/{id}")
    TeacherDTO getTeacher(@PathVariable("id") Integer id);

    @GetMapping("/{id}")
    Boolean saveTeacher(@PathVariable("id") TeacherDTO teacherDTO);

    @GetMapping("/{id}")
    Boolean updateTeacher(@PathVariable("id") TeacherDTO teacherDTO);

    @GetMapping("/{id}")
    Boolean deleteTeacher(@PathVariable("id") Integer id);

    @GetMapping("/{id}")
    Boolean insertTeacherList(List<TeacherDTO> teacherDTO);

    //校验教师存不存在，传入要查询的教师卡号，如果存在返回ture，不存在返回false。
    @GetMapping("/{teacherNo}")
    Boolean checkTeacherExist(@PathVariable("teacherNo") String teacherNo);

    //查询教师所授课程的平均成绩
    @GetMapping("/{teacherNo}")
    int getAvgScore(@PathVariable("teacherNo") String teacherNo);

}
