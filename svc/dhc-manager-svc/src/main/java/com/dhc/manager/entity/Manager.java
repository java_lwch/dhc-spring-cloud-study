package com.dhc.manager.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_manager")
public class Manager extends Model<Manager> {

    @TableId(value = "manager_id", type = IdType.AUTO)
    Integer managerId;

    @TableField("manager_name")
    String managerName;

    @TableField("user_id")
    Integer userId;
}