package com.dhc.student.dto;

import lombok.Data;

@Data
public class StudentAvgScore {

    //平均成绩
    private String score;
}
