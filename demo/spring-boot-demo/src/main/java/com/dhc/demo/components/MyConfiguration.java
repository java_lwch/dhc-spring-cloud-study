package com.dhc.demo.components;

import com.dhc.demo.dto.OrderDTO;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liwencheng
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "dhc.test")
public class MyConfiguration {

    private String name;
    private String value;
}
