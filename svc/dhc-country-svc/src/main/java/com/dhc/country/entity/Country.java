package com.dhc.country.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.extension.activerecord.Model;


@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_country")
public class Country extends Model<Country> {
    //国家id
    @TableId(value = "Country_id", type = IdType.AUTO)
    private Integer CountryId;
    //国家No
    @TableField("Country_no")
    private String CountryNo;
    //国家名
    @TableField("Country_name")
    private String CountryName;
}