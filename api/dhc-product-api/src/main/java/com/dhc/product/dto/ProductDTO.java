package com.dhc.product.dto;

import lombok.Data;

/**
 * @author liwencheng
 */
@Data
public class ProductDTO {

    private Integer id;
    private String productNo;
    private String productName;
}
