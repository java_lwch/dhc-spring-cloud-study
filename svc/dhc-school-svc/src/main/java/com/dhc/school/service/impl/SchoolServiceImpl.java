package com.dhc.school.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.school.Utils.CheckUtils;
import com.dhc.school.dto.SchoolDTO;
import com.dhc.school.entity.School;
import com.dhc.school.mapper.SchoolMapper;
import com.dhc.school.service.SchoolService;
import com.sun.istack.internal.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author HanwenZhang
 * @version 1.0
 */
@Service
public class SchoolServiceImpl extends ServiceImpl<SchoolMapper, School> implements SchoolService {

    @Autowired
    private SchoolMapper schoolMapper;

    /**
     * 查询所有学校的信息
     * @return 所有学校的结果集合
     */
    @Override
    public List<School> listSchool() {
        return schoolMapper.selectSchoolList();
    }

    /**
     * 添加学校的全部信息
     * @param schoolDTO
     * @return 是否更新成功
     */
    @Override
    public Boolean saveSchool(SchoolDTO schoolDTO) {
        //校验城市为不为空
//        City city=CityMapper.selectById(CityDTO.getId());
//        if(order==null){
//            return false;
//        }

        //判断id是否已经存在，存在返回false
        if(!CheckUtils.IsSchoolIdExist(schoolDTO)){
            return false;
        }
        School school = new School();
        BeanUtils.copyProperties(schoolDTO, school);
        return school.insert();
    }

    /**
     * 更新学校的全部信息
     * @param schoolDTO
     * @return 是否更新成功
     */
    @Override
    public Boolean updateSchool(SchoolDTO schoolDTO) {
//校验城市为不为空
//        City city=CityMapper.selectById(CityDTO.getId());
//        if(city==null){
//            return false;
//        }
        //判断要修改的信息id是否存在，存在返回true
        if(CheckUtils.IsSchoolIdExist(schoolDTO)){
            return false;
        }
        School school=new School();
        return school.update(new UpdateWrapper<School>()
                .set("School_type", schoolDTO.getSchoolType())
                .set("School_name", schoolDTO.getSchoolName())
                .set("School_Email", schoolDTO.getSchoolEmail())
                .set("School_Address", schoolDTO.getSchoolAddress())
                .set("city_id", schoolDTO.getCityId())
                .eq("School_id", schoolDTO.getSchoolId()));
    }

    /**
     * 通过学校id进行删除
     * @param SchoolId 学校id
     * @return 是否删除成功
     */
    @Override
    public Boolean deleteSchool(Integer SchoolId) {
        //校验班级为不为空
//        Classes c=ClassesMapper.selectById(ClassesDTO.getId());
//        if (c==null){
//            return false;
//        }
        return super.removeById(SchoolId);
    }

    /**
     * 根据学校id进行查询数据
     * @param SchoolId 学校Id
     * @return 查询结果的集合
     */
    @Override
    public School selectSchoolById(Integer SchoolId){
        return schoolMapper.selectSchoolById(SchoolId);
    }

    /**
     * 通过学校名字进行模糊查询
     * @param SchoolName 学校的名字
     * @return 查询结果的集合
     */
    @Override
    public  List<School> listSchoolByName(String SchoolName){
        return schoolMapper.getSchoolByName(SchoolName);
    }
}