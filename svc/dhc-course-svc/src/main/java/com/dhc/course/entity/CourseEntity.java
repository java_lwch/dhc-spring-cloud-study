package com.dhc.course.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

@EqualsAndHashCode(callSuper = true)
@Data
@TableName("t_course")
public class CourseEntity extends Model<CourseEntity> {


    @TableId(value = "course_id")
    private Integer courseId;

    @TableField(value = "course_name")
    private String  courseName;

    @TableField(value = "school_id")
    private Integer schoolId;

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }
}
