package com.dhc.gateway.filter;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.dhc.gateway.com.dhc.gateway.utils.JWTUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;

@Component
@Slf4j
public class LoginFilter implements GlobalFilter, Ordered {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String token;
        try{
            token = exchange.getRequest().getQueryParams().get("token").get(0);
        }catch(NullPointerException e){
            return createResponse(exchange);
        }

        log.info("token="+token);
        if(token == null){
            if(exchange.getRequest().getURI().getPath().indexOf("/user/login")>0 ||
               exchange.getRequest().getURI().getPath().indexOf("/user/register")>0)
            {
                return chain.filter(exchange);
            }else {
                return createResponse(exchange);
            }
        }else{
            //验证登录
            //使用秘钥生成匹配KEY
            try {
                DecodedJWT tokenVerify = JWTUtils.verify(token);
                return chain.filter(exchange);
            }catch(SignatureVerificationException e){
                log.info("SignatureVerificationException");
                e.printStackTrace();
            }catch(TokenExpiredException e){
                log.info("TokenExpiredException");
                e.printStackTrace();

            }catch(AlgorithmMismatchException e){
                log.info("AlgorithmMismatchException");
                e.printStackTrace();

            }catch (Exception e){
                log.info("Exception");
                e.printStackTrace();

            }

            return createResponse(exchange);
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }

    private Mono<Void> createResponse(ServerWebExchange exchange){

        ServerHttpResponse response = exchange.getResponse();
        JSONObject message = new JSONObject();
        message.put("state", false);
        message.put("msg", "令牌认证失败，请登录:http://localhost:9999/user/login或请注册:http://localhost:9999/user/register");
        byte[] bits = message.toJSONString().getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bits);
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        response.getHeaders().add("Content-Type", "text/plain;charset=UTF-8");
        return response.writeWith(Mono.just(buffer));
    }
}
