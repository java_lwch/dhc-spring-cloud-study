package com.dhc.student.service;

import com.dhc.student.dto.StudentDTO;
import com.dhc.student.entity.Student;

import java.util.List;

public interface StudentService {
    List<Student> listStudent();

    Student getStudent(Integer sid);

    Boolean saveStudent(StudentDTO studentDTO);

    Boolean updateStudent(StudentDTO studentDTO);

    Boolean deleteStudent(Integer sid);

    Boolean insertStudentList(List<StudentDTO> studentDTO);

    List<Student> selectByName(String name);
}
