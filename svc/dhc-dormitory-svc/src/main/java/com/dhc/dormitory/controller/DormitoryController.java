package com.dhc.dormitory.controller;

import com.dhc.dormitory.dto.DormitoryDTO;
import com.dhc.dormitory.entity.Dormitory;
import com.dhc.dormitory.service.DormitoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author wangyijun
 */

@RestController
@RequestMapping("/dormitory")
public class DormitoryController {

    @Autowired
    private DormitoryService dormSVC;

    /**
     * 查询所有宿舍信息
     * @return 宿舍结果集
     */
    @GetMapping("/queryAll")
    public List<Dormitory> getAllDorm(){
        return dormSVC.getAllDorm();
    }

    /**
     * 通过宿舍ID查询宿舍信息
     * @param 宿舍ID
     * @return 宿舍结果
     */
    @GetMapping("/query/{dormId}")
    public Dormitory getDormByID(@PathVariable("dormId")String dormitoryId){
        return dormSVC.getDormByID(dormitoryId);
    }

    /**
     * 插入1条新的宿舍记录
     * @return 更新是否成功
     */
    @PostMapping("/add")
    public Boolean addDorm(@RequestBody DormitoryDTO dormDTO){
        return dormSVC.addDorm(dormDTO);
    }

    /**
     * 删除1条宿舍记录
     * @return 更新是否成功
     */
    @DeleteMapping("delete/{dormId}")
    public Boolean delDorm(@PathVariable("dormId")String dormitoryId){
        return dormSVC.delDorm(dormitoryId);
    }

    /**
     * 更新1条宿舍记录
     * @return 更新是否成功
     */
    @PostMapping("/update")
    public Boolean updDorm(@RequestBody DormitoryDTO dormDTO){
        return dormSVC.updDorm(dormDTO);
    }
}
