package com.dhc.product.fallback;

import com.dhc.product.api.IProductApi;
import com.dhc.product.dto.ProductDTO;
import org.springframework.stereotype.Component;

/**
 * @author liwencheng
 */
@Component
public class IProductApiFallBack implements IProductApi {

    /**
     * 根据id 查询商品
     *
     * @param id 商品id
     * @return 商品信息
     */
    @Override
    public ProductDTO getProduct(Integer id) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setProductName("fallBack");
        productDTO.setProductNo("fallBack");
        return productDTO;
    }
}
