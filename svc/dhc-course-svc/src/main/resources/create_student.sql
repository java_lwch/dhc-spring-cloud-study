CREATE TABLE `student` (
	`student_id` INT(3) NOT NULL,
	`school_no` CHAR(1) NULL DEFAULT NULL,
	`student_class` CHAR(10) NULL DEFAULT NULL,
	`student_name` VARBINARY(50) NOT NULL DEFAULT '''',
	`student_sex` VARBINARY(50) NOT NULL DEFAULT '''',
	PRIMARY KEY (`student_id`)
)
