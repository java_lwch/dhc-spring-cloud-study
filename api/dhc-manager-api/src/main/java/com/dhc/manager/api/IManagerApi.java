package com.dhc.manager.api;

import com.dhc.manager.dto.ManagerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "dhc-manager-svc", path = "/manager")
public interface IManagerApi {
    @GetMapping("/select")
    public List<ManagerDTO> selectAll();

}
