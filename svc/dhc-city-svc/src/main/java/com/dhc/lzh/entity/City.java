package com.dhc.scy.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
@TableName("student")
public class City extends Model<City> {
    //城市编码
    @TableField("no")
    private Integer no;

    //城市名字
    @TableField("name")
    private Integer name;
}
