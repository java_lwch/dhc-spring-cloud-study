package com.dhc.score.api;

import com.dhc.score.dto.ScoreDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author dongjianmin
 */
@FeignClient(name = "dhc-score-svc", path = "/score")
public interface IScoreApi {

    /**
     * 根据id 查询成绩
     * @param 学生sid，课程id
     * @return 成绩信息
     */
    @GetMapping("/{ScoreSid}/{ScoreCid}")
    ScoreDTO selectScoreById(@PathVariable("ScoreSid")Integer ScoreSid,@PathVariable("ScoreCid")Integer ScoreCid);

    /**
     * 根据id 查询成绩
     * @param 学生sid
     * @return 成绩信息集合
     */
    @GetMapping("/{ScoreSid}")
    List<ScoreDTO> selectScoreBySid(@PathVariable("ScoreSid")Integer ScoreSid);
}
