package com.dhc.dorm.dto;

import lombok.Data;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/10 23:55
 */
@Data
public class DormDTO {
    private Integer dorm_id;
    private Integer dorm_no;
    private String dorm_name;
}
