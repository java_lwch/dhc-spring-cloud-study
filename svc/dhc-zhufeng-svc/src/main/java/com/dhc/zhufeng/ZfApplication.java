package com.dhc.zhufeng;


import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringCloudApplication
@EnableFeignClients("com.dhc")
public class ZfApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZfApplication.class, args);
    }
}
