package com.dhc.studentcourse.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhc.studentcourse.entity.CourseEntity;
import com.dhc.studentcourse.entity.StudentCourseEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CourseMapper extends BaseMapper<CourseEntity> {

     CourseEntity selectCourse(Integer courseId);

}
