package com.dhc.book.controller;

import com.dhc.book.entity.Book;
import com.dhc.book.service.impl.BookServiceImpl;
import com.dhc.book.dto.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/book")
@RefreshScope
public class BookController {

    @Autowired
    BookServiceImpl bookService;

    @GetMapping("/book")
    public List<Book> selectAll() {

        return bookService.selectAll();
    }

    @PostMapping
    public Boolean saveBook(@RequestBody BookDTO bookDTO){
        return bookService.saveBook(bookDTO);
    }

    @PutMapping
    public Boolean updateBook(@RequestBody BookDTO bookDTO){
        return bookService.updateBook(bookDTO);
    }

    @DeleteMapping("/{BookId}")
    public Boolean deleteBook(@PathVariable("BookId")Integer BookId){
        return bookService.deleteBook(BookId);
    }

}
