package com.dhc.dorm.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.dorm.dto.DormDTO;
import com.dhc.dorm.entity.Dorm;
import com.dhc.dorm.entity.Teacher;
import com.dhc.dorm.mapper.DormMapper;
import com.dhc.dorm.service.DormService;
import com.dhc.dorm.entity.Student;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZiyuanSong
 * @version 1.0
 * @date 2020/11/11 22:59
 */
@Service
public class DormServiceImpl extends ServiceImpl<DormMapper, Dorm> implements DormService {
    @Autowired
    private DormMapper dormMapper;

    @Override
    public List<Dorm> listDorm() {
        return dormMapper.selectDormList();
    }

    @Override
    public Dorm getDorm(Integer dorm_id) {
        Dorm dorm = new Dorm();
        dorm.selectById(dorm_id);
        return dormMapper.selectById(dorm_id);
    }

    @Override
    public Boolean saveDorm(DormDTO dormDTO) {
        Dorm dorm = new Dorm();
        BeanUtils.copyProperties(dormDTO, dorm);
        return dorm.insert();
    }

    @Override
    public Boolean updateDorm(DormDTO dormDTO) {
        Dorm dorm = new Dorm();
        return dorm.update(new UpdateWrapper<Dorm>()
                .eq("dorm_id", dormDTO.getDorm_id())
                .set("dorm_no", dormDTO.getDorm_no())
                .set("dorm_name", dormDTO.getDorm_name()));
    }

    @Override
    public Boolean deleteDorm(int dorm_id) {
        return super.removeById(dorm_id);
    }


    @Override
    public Teacher selectTeacherWithStudentWithTheHighestScoreInSpecifiedCourse(String course_name) {
        return dormMapper.selectTeacherWithStudent(course_name);
    }

    @Override
    public List<Student> selectAvgFromDrom(int dorm_id) {
        return dormMapper.selectAvgFromDrom(dorm_id);
    }
}
