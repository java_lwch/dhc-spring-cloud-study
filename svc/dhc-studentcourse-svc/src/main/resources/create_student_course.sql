CREATE TABLE `student_course` (
  `student_id` int(3) NOT NULL,
  `course_id` int(4) NOT NULL,
  `student_name` char(20) DEFAULT NULL,
  `student_sex` char(1) DEFAULT NULL,
  `student_class` char(10) DEFAULT NULL,
  `course_name` char(20) NOT NULL,
  PRIMARY KEY (`student_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci