package com.dhc.user_role.service;

import com.dhc.user_role.entity.Role;
import com.dhc.user_role.mapper.RoleMapper;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Data
public class RoleServiceImp implements RoleService{

    @Autowired
    public RoleMapper roleMapper;

    /**
     * 根据角色id查询角色信息
     * @param id 角色id
     * @return 角色信息
     */
    @Transactional
    @Override
    public Role getRoleById(Integer id) {
        return roleMapper.getRoleById(id);
    }
}
