package com.dhc.user.dto;

import lombok.Data;

/**
 * @author liwencheng
 */
@Data
public class UserDTO {

    private Integer id;

    private String username;

    private String password;
}
