package com.dhc.teacher.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@TableName("t_teacher")
@Data

public class Teacher extends Model<Teacher> {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("teacher_no")
    private String teacherNo;

    @TableField("teacher_name")
    private String teacherName;

    @TableField("teacher_sex")
    private String teacherSex;

    @TableField("teacher_age")
    private Integer teacherAge;

    @TableField("teacher_salary")
    private Integer teacherSalary;

    @TableField("school_no")
    private String schoolNo;


}
