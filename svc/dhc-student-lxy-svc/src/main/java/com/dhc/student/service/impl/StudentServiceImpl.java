package com.dhc.student.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhc.student.entity.Student;
import com.dhc.student.mapper.StudentMapper;
import com.dhc.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {
    @Autowired
    StudentMapper studentMapper;

    @Override
    public Double selectAvgPoint(String studentName){

        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaa"+studentMapper.selectStudentPoint(studentName));

        return studentMapper.selectStudentPoint(studentName);
    }
}
